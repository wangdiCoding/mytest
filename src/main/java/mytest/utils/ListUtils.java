package mytest.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author dwang
 * @Company neldtv
 * @Description list utils
 * @create 2021/6/9 9:07
 * @Modified By:
 */
public class ListUtils {
    public static List getList(String ...strArr) {
        List<String> stringList = new ArrayList<>();
        for (String s : strArr) {
            stringList.add(s);
        }
        return stringList;
    }
}
