package mytest.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 执行shell命令
 * @create 2021/6/18 14:52
 * @Modified By:
 */
public class ProcessUtil {

    public static String execShell(String exec, String[] shell) {
        StringBuilder result = new StringBuilder();
        Process process = null;
        BufferedReader bufferedReaderInfo = null;
        BufferedReader bufferedReaderError = null;
        try {
            // 执行shell命令，返回了一个进程
            if (StringUtils.isEmpty(exec)) {
                process = Runtime.getRuntime().exec(shell);

            } else {
                process = Runtime.getRuntime().exec(exec);

            }

            // 读取流信息，不然会阻塞
            ThreadPoolUtils.execute(new StreamRunable(process.getInputStream(), "input "));
            ThreadPoolUtils.execute(new StreamRunable(process.getErrorStream(), "error "));


            // 等待命令执行完成
            process.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 释放资源
            if (bufferedReaderError!=null){
                try {
                    bufferedReaderError.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufferedReaderInfo!=null){
                try {
                    bufferedReaderInfo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (process!=null){
                process.destroy();
            }

        }
        return result.toString();
    }


}
