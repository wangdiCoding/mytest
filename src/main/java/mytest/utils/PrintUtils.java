package mytest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 打印格式输出
 * @create 2021/6/25 9:10
 * @Modified By:
 */
public class PrintUtils {

    private PrintUtils() {
    }

    private static Logger logger = LoggerFactory.getLogger(PrintUtils.class);

    public static void printList(Collection collection, String collectionName){

        logger.debug("--------------------------------{} print--------------------------------", collectionName);
        collection.forEach(item->logger.debug(item.toString()));
        logger.debug("--------------------------------{} print end--------------------------------", collectionName);
        logger.debug("");
    }

}
