package mytest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 任务
 * @create 2021/6/22 11:21
 * @Modified By:
 */
public class StreamRunable implements Runnable {
    private InputStream inputStream;
    private String name;
    static Logger logger = LoggerFactory.getLogger(StreamRunable.class);

    public StreamRunable(InputStream in, String name) {
        this.inputStream = in;
        this.name = name;
    }

    @Override
    public void run() {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        try {
            while ((line = in.readLine()) != null) {
                logger.debug(name + "stream result: " + line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
