package mytest.utils;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 简单线程池使用
 * @create 2021/6/22 13:56
 * @Modified By:
 */
public class ThreadPoolUtils {
    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 10, 1000, TimeUnit.SECONDS, new ArrayBlockingQueue(10));

    public static void execute(Runnable runnable) {
        threadPoolExecutor.execute(runnable);
    }
}
