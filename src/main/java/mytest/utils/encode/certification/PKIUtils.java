package mytest.utils.encode.certification;

import mytest.utils.encode.rsa.RSAHelper2;
import org.bouncycastle.asn1.pkcs.CertificationRequestInfo;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 模拟PKI 下发证书
 * @create 2021/6/9 17:30
 * @Modified By:
 */
public class PKIUtils {

    private static HashMap crl = new HashMap();

    /**
     * pki对公钥进行签名
     * @param publicKey
     * @param name
     * @return
     */
    public static CustomCertificatInfo register(RSAPublicKey publicKey, String name) {

        crl.put(name, publicKey);

        // bob 公钥转成str保存
        byte[] publicKeybyte = publicKey.getEncoded();
        String publicKeyString = java.util.Base64.getEncoder().encodeToString(publicKeybyte);

        // 生成自己的证书
        RSAHelper2.CustomKeyPairInfo CAKeyPair = RSAHelper2.getKeyPair();

        // CA 私钥加密publicKey生成签名
        RSAPrivateKey caPrivateKey = CAKeyPair.getPrivateKey();
        String signature = RSAHelper2.encipher(publicKeyString, caPrivateKey, RSAHelper2.KEY_SIZE / 8 - 11);

        // 生成证书
        // 1. 签名，2.bob的公钥 3.CA的公钥
        CustomCertificatInfo customCertificatInfo = new CustomCertificatInfo(signature, CAKeyPair.getPublicKey(),
                publicKey);

        return customCertificatInfo;
    }
}
