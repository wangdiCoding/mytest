package mytest.utils.encode.certification;
import	java.security.PublicKey;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 包装证书信息
 * @create 2021/6/9 18:11
 * @Modified By:
 */
public class CustomCertificatInfo {
    private String caSignature;
    private PublicKey caPublicKey;
    private PublicKey clientPublicKey;

    public CustomCertificatInfo() {
    }

    public CustomCertificatInfo(String caSignature, PublicKey caPublicKey, PublicKey clientPublicKey) {
        this.caSignature = caSignature;
        this.caPublicKey = caPublicKey;
        this.clientPublicKey = clientPublicKey;
    }

    public String getCaSignature() {
        return caSignature;
    }

    public PublicKey getCaPublicKey() {
        return caPublicKey;
    }

    public PublicKey getClientPublicKey() {
        return clientPublicKey;
    }
}
