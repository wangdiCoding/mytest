package mytest.utils.encode.signiture;
import	java.security.PublicKey;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 包装签名信息
 * @create 2021/6/9 17:02
 * @Modified By:
 */
public class SignitureInfo {
    private String signiture;
    private PublicKey publicKey;

    public SignitureInfo(String signiture, PublicKey publicKey) {
        this.signiture = signiture;
        this.publicKey = publicKey;
    }

    public String getSigniture() {
        return signiture;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }
}
