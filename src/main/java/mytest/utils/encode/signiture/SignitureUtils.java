package mytest.utils.encode.signiture;

import mytest.utils.encode.hash.SHAUtils;
import mytest.utils.encode.rsa.RSAHelper2;

import java.security.interfaces.RSAPrivateKey;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 签名工具
 * @create 2021/6/9 16:54
 * @Modified By:
 */
public class SignitureUtils {
    public static SignitureInfo getSignitureAndPublicKey(String msg) {
        // 对消息进行单向散列函数处理
        String hashValue = SHAUtils.getSHA256(msg);
        System.out.println("散列后的消息为：" + hashValue);


        // 散列值进行私钥加密
        RSAHelper2.CustomKeyPairInfo keyPair = RSAHelper2.getKeyPair();
        RSAPrivateKey privateKey = keyPair.getPrivateKey();
        String signature = RSAHelper2.encipher(hashValue, privateKey, RSAHelper2.KEY_SIZE / 8 - 11);

        // 公钥和签名给接收方
        SignitureInfo signitureInfo = new SignitureInfo(signature, keyPair.getPublicKey());

        return signitureInfo;
    }

}
