package mytest.utils.encode.aes;

/**
 * @Description
 * @ClassName AES256Utils
 * @Author wangDi
 * @date 2021-04-15 17:12
 */

import mytest.utils.encode.EncrypyUtil;
import mytest.utils.encode.hash.SHAUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;
import java.util.Base64;

public class AES256Utils {

    private final static Logger logger = LoggerFactory.getLogger(AES256Utils.class);

    /***默认向量常量**/
    public static byte[] IV = null;
    // "算法/加密/填充"
    public static final String CIPHER_INIT = "AES/CFB/PKCS7Padding";

    /**
     * 使用PKCS7Padding填充必须添加一个支持PKCS7Padding的Provider
     * 类加载的时候就判断是否已经有支持256位的Provider,如果没有则添加进去
     */
    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * 加密 128位
     *
     * @param content 需要加密的原内容
     * @param pkey    密匙
     * @return
     */
    public static byte[] aesEncrypt(String content, String pkey) {
        try {
            SecretKeySpec skey = new SecretKeySpec(pkey.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance(CIPHER_INIT);
            // 偏移量要相同
            IvParameterSpec iv = new IvParameterSpec(getIV());
            cipher.init(Cipher.ENCRYPT_MODE, skey, iv);//初始化加密器
            byte[] encrypted = cipher.doFinal(content.getBytes("UTF-8"));
            return encrypted; // 加密
        } catch (Exception e) {
            logger.info("aesEncrypt() method error:", e);
        }
        return null;
    }

    /**
     * 获取初始化向量:随机生成
     * @return
     */
    private static byte[] getIV() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = new byte[16];
        secureRandom.nextBytes(bytes);
        IV = bytes;
        return bytes;
    }

    /**
     * 输入seed,获取伪随机数
     *
     * @param seed
     * @return
     * @throws Exception
     */
    private static SecretKey generateKey(String seed) throws Exception {
        //防止linux下 随机生成key
        Provider p = Security.getProvider("SUN");
        // 单向散列法生成伪随机数
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", p);
        secureRandom.setSeed(seed.getBytes());
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        kg.init(secureRandom);
        // 生成密钥
        return kg.generateKey();
    }

    /**
     * @param content 加密前原内容
     * @param pkey    长度为16个字符,128位
     * @return base64EncodeStr   aes加密完成后内容
     * @throws
     * @Title: aesEncryptStr
     * @Description: aes对称加密
     */
    public static String aesEncryptStr(String content, String pkey) {
        byte[] aesEncrypt = aesEncrypt(content, pkey);
        System.out.println("加密后的byte数组:" + Arrays.toString(aesEncrypt));
        String base64EncodeStr = Base64.getEncoder().encodeToString(aesEncrypt);
        System.out.println("加密后 base64EncodeStr:" + base64EncodeStr);
        return base64EncodeStr;
    }

    /**
     * @param content base64处理过的字符串
     * @param pkey    密匙
     * @return String    返回类型
     * @throws Exception
     * @throws
     * @Title: aesDecodeStr
     * @Description: 解密 失败将返回NULL
     */
    public static String aesDecodeStr(String content, String pkey) {
        try {
            System.out.println("待解密内容:" + content);
            byte[] base64DecodeStr = Base64.getDecoder().decode(content);
            System.out.println("base64DecodeStr:" + Arrays.toString(base64DecodeStr));
            byte[] aesDecode = aesDecode(base64DecodeStr, pkey);
            System.out.println("aesDecode:" + Arrays.toString(aesDecode));
            if (aesDecode == null) {
                return null;
            }
            String result;
            result = new String(aesDecode, "UTF-8");
            System.out.println("aesDecode result:" + result);
            return result;
        } catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        return null;
    }

    /**
     * 解密 128位
     *
     * @param content 解密前的byte数组
     * @param pkey    密匙
     * @return result  解密后的byte数组
     * @throws Exception
     */
    public static byte[] aesDecode(byte[] content, String pkey) throws Exception {

        SecretKeySpec skey = new SecretKeySpec(pkey.getBytes(), "AES");
        IvParameterSpec iv = new IvParameterSpec(IV);
        Cipher cipher = Cipher.getInstance(CIPHER_INIT);// 创建密码器
        cipher.init(Cipher.DECRYPT_MODE, skey, iv);// 初始化解密器
        byte[] result = cipher.doFinal(content);
        return result; // 解密

    }


    /**
     * 获取密钥
     * @param seed
     * @return
     */
    private String getKey(String seed) {
        try {
            SecretKey secretKey1 = generateKey(seed);
            byte[] encoded = secretKey1.getEncoded();
            // 补充
            String key = EncrypyUtil.byte2Hex(encoded);
            return key;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 第一步：获取密钥：
     * 第二步： 进行加密
     *      加密选择分组加密的模式，填充模式
     *
     * @throws Exception
     */
    public void test(String content) throws Exception {


        //盐值也应该随机
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = secureRandom.generateSeed(16);
        String seed = new String(bytes);


        String pkey = getKey(seed);
        System.out.println("待加密报文:" + content);
        System.out.println("密匙:" + pkey);


        String aesEncryptStr = aesEncryptStr(content, pkey);

        String aesDecodeStr = aesDecodeStr(aesEncryptStr, pkey);
        System.out.println("解密报文:" + aesDecodeStr);
        System.out.println("加解密前后内容是否相等:" + aesDecodeStr.equals(content));
    }
}


