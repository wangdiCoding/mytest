package mytest.utils.encode;


import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;

/**
 * @Description AES 加解密， 密钥被RSA加解密
 * @ClassName EncryptyUtil
 * @Author wangDi
 * @date 2021-04-12 13:51
 */
public class EncrypyUtil {

    public static final int kBufferSize = 8192;
    public static String inPath = "E:\\in.txt";
    public static String outPath = "E:\\out.txt";
    public static String outPath2 = "E:\\out2.txt";
    public static IvParameterSpec ivParameterSpec = null;

    public static char[] passWd = "123456".toCharArray();
    /**
     * 字节数组转16进制字符串
     * @param digest
     * @return
     */
    public static String byte2Hex(byte[] digest) {
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i = 0; i < digest.length; i++) {
            temp = Integer.toHexString(digest[i] & 0xFF);
            if (temp.length() == 1) {
                // 补零
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }


    /**
     * 16进制字符串转字节数组
     */
    public static byte[] hexToByte(String hex) {
        int byteLength = hex.length() / 2;
        byte[] bytes = new byte[byteLength];
        int m = 0;
        int n = 0;
        /**
         * 每次取两个转成一个字节
         */
        for (int i = 0; i < byteLength; i++) {
            m = i * 2 + 1;
            n = m + 1;
            int intHex = Integer.decode("0x" + hex.substring(i * 2, m) + hex.substring(m, n));
            bytes[i] = Byte.valueOf((byte) intHex);
        }
        return bytes;
    }

    public static void main(String[] args) {
        String string ="hello wd";
        byte[] bytes = string.getBytes();
        String s = EncrypyUtil.byte2Hex(bytes);
        System.out.println("hex: " + s);

        byte[] bytes1 = EncrypyUtil.hexToByte(s);

        String s1 = new String(bytes1);
        System.out.println(s1);


    }

    public static IvParameterSpec getIV(String input) {
        SecureRandom secureRandom = new SecureRandom(input.getBytes());
        if (ivParameterSpec != null) {
            return ivParameterSpec;
        }
        byte[] IV = new byte[16];
//        secureRandom.nextBytes(IV);
        ivParameterSpec = new IvParameterSpec(IV);
        return ivParameterSpec;
    }


}
