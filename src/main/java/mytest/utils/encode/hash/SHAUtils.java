package mytest.utils.encode.hash;

import mytest.utils.encode.EncrypyUtil;

import java.io.UnsupportedEncodingException;
import java.security.*;

/**
 * @Author dwang
 * @Company neldtv
 * @Description secure hash algorithm  单向散列生成
 *  * @create 2021/6/9 10:42
 * @Modified By:
 */
public class SHAUtils {

    public static void main(String[] args) {
        String str = "test";
        String sha256 = getSHA256(str);

    }

    public static String getSHA256(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes("UTF-8"));
            byte[] digest = instance.digest();

            String string = EncrypyUtil.byte2Hex(digest);

            return string;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
