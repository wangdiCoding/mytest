package mytest.utils.encode.mac;

import mytest.utils.encode.EncrypyUtil;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * @Author dwang
 * @Company neldtv
 * @Description Hash 实现HMAC
 * @create 2021/6/9 16:18
 * @Modified By:
 * 参考：https://www.cnblogs.com/fishou/p/4159092.html
 */
public class HMacUtil {
    public static final String MAC_NAME = "HmacSHA512";


    public static String getHMAC(String message, Key key) {
        try {
            byte[] msgBytes = message.getBytes();

            Mac mac = Mac.getInstance(MAC_NAME);
            mac.init(key);
            byte[] bytes = mac.doFinal(msgBytes);


            String macValue = EncrypyUtil.byte2Hex(bytes);
            System.out.println("mac值为 " + macValue + " 长度为： " + macValue.length());
            return macValue;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

}

