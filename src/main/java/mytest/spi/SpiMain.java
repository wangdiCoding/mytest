package mytest.spi;

import java.util.ServiceLoader;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/1/22 14:11
 * @Modified By:
 */
public class SpiMain {

    public static void main(String[] args) {

        ServiceLoader<IShout> shouts = ServiceLoader.load(IShout.class);
        for (IShout s : shouts) {
            s.shout();
        }
    }
}
