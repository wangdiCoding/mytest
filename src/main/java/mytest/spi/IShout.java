package mytest.spi;


/**
 * @Author dwang
 * @Description spi实现类加载
 * @create 2022/1/22 14:08
 * @Modified By:
 */
public interface IShout {

    void shout();
}
