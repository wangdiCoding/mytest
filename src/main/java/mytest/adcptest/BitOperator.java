package mytest.adcptest;

import java.util.Arrays;
import java.util.List;

/** 数组处理工具类 */
public class BitOperator {

  public static byte integerTo1Byte(int value) {
    return (byte) (value & 0xFF);
  }

  /**
   * 把一个整形该为1位的byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] integerTo1Bytes(int value) {
    byte[] result = new byte[1];
    result[0] = (byte) (value & 0xFF);
    return result;
  }

  /**
   * 把一个整形改为2位的byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] integerTo2Bytes(int value) {
    byte[] result = new byte[2];
    result[0] = (byte) ((value >>> 8) & 0xFF);
    result[1] = (byte) (value & 0xFF);
    return result;
  }

  /**
   * 把一个整形改为3位的byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] integerTo3Bytes(int value) {
    byte[] result = new byte[3];
    result[0] = (byte) ((value >>> 16) & 0xFF);
    result[1] = (byte) ((value >>> 8) & 0xFF);
    result[2] = (byte) (value & 0xFF);
    return result;
  }

  /**
   * 把一个整形改为4位的byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] integerTo4Bytes(int value) {
    byte[] result = new byte[4];
    result[0] = (byte) ((value >>> 24) & 0xFF);
    result[1] = (byte) ((value >>> 16) & 0xFF);
    result[2] = (byte) ((value >>> 8) & 0xFF);
    result[3] = (byte) (value & 0xFF);
    return result;
  }

  /**
   * 把byte[]转化位整形,通常为指令用
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static int byteToInteger(byte[] value) {
    int result;
    if (value.length == 1) {
      result = oneByteToInteger(value[0]);
    } else if (value.length == 2) {
      result = twoBytesToInteger(value);
    } else if (value.length == 3) {
      result = threeBytesToInteger(value);
    } else if (value.length == 4) {
      result = fourBytesToInteger(value);
    } else {
      result = fourBytesToInteger(value);
    }
    return result;
  }

  public static int parsIntFromBytes(byte[] data, int startIndex, int length) {
    try {
      final int len = length > 4 ? 4 : length;
      byte[] tmp = new byte[len];
      System.arraycopy(data, startIndex, tmp, 0, len);
      return BitOperator.byteToInteger(tmp);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  /**
   * 把一个byte转化位整形,通常为指令用
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static int oneByteToInteger(byte value) {
    return (int) value & 0xFF;
  }

  /**
   * 把一个2位的数组转化位整形
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static int twoBytesToInteger(byte[] value) {
    // if (value.length < 2) {
    // throw new Exception("Byte array too short!");
    // }
    int temp0 = value[0] & 0xFF;
    int temp1 = value[1] & 0xFF;
    return ((temp0 << 8) + temp1);
  }

  /**
   * 把一个3位的数组转化位整形
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static int threeBytesToInteger(byte[] value) {
    int temp0 = value[0] & 0xFF;
    int temp1 = value[1] & 0xFF;
    int temp2 = value[2] & 0xFF;
    return ((temp0 << 16) + (temp1 << 8) + temp2);
  }

  /**
   * 把一个4位的数组转化位整形,通常为指令用
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static int fourBytesToInteger(byte[] value) {
    // if (value.length < 4) {
    // throw new Exception("Byte array too short!");
    // }
    int temp0 = value[0] & 0xFF;
    int temp1 = value[1] & 0xFF;
    int temp2 = value[2] & 0xFF;
    int temp3 = value[3] & 0xFF;
    return ((temp0 << 24) + (temp1 << 16) + (temp2 << 8) + temp3);
  }

  /**
   * 把一个4位的数组转化位整形
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static long fourBytesToLong(byte[] value) throws Exception {
    // if (value.length < 4) {
    // throw new Exception("Byte array too short!");
    // }
    int temp0 = value[0] & 0xFF;
    int temp1 = value[1] & 0xFF;
    int temp2 = value[2] & 0xFF;
    int temp3 = value[3] & 0xFF;
    return (((long) temp0 << 24) + (temp1 << 16) + (temp2 << 8) + temp3);
  }

  /**
   * 把一个数组转化长整形
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static long bytes2Long(byte[] value) {
    long result = 0;
    int len = value.length;
    int temp;
    for (int i = 0; i < len; i++) {
      temp = (len - 1 - i) * 8;
      if (temp == 0) {
        result += (value[i] & 0x0ff);
      } else {
        result += (value[i] & 0x0ff) << temp;
      }
    }
    return result;
  }

  /**
   * 把一个长整形改为byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] longToBytes(long value) {
    return longToBytes(value, 8);
  }

  /**
   * 把一个长整形改为byte数组
   *
   * @param value
   * @return
   * @throws Exception
   */
  public static byte[] longToBytes(long value, int len) {
    byte[] result = new byte[len];
    int temp;
    for (int i = 0; i < len; i++) {
      temp = (len - 1 - i) * 8;
      if (temp == 0) {
        result[i] += (value & 0x0ff);
      } else {
        result[i] += (value >>> temp) & 0x0ff;
      }
    }
    return result;
  }

  /**
   * 把IP拆分位int数组
   *
   * @param ip
   * @return
   * @throws Exception
   */
  public static int[] getIntIPValue(String ip) throws Exception {
    String[] sip = ip.split("[.]");
    // if (sip.length != 4) {
    // throw new Exception("error IPAddress");
    // }
    int[] intIP = {
      Integer.parseInt(sip[0]),
      Integer.parseInt(sip[1]),
      Integer.parseInt(sip[2]),
      Integer.parseInt(sip[3])
    };
    return intIP;
  }

  /**
   * 把byte类型IP地址转化位字符串
   *
   * @param address
   * @return
   * @throws Exception
   */
  public static String getStringIPValue(byte[] address) throws Exception {
    int first = oneByteToInteger(address[0]);
    int second = oneByteToInteger(address[1]);
    int third = oneByteToInteger(address[2]);
    int fourth = oneByteToInteger(address[3]);

    return first + "." + second + "." + third + "." + fourth;
  }

  /**
   * 合并字节数组
   *
   * @param first
   * @param rest
   * @return
   */
  public static byte[] concatAll(byte[] first, byte[]... rest) {
    if (first == null) {
      int totalLength = 0;
      for (byte[] array : rest) {
        if (array != null) {
          totalLength += array.length;
        }
      }
      byte[] result = new byte[totalLength];
      int offset = 0;
      for (byte[] array : rest) {
        if (array != null) {
          System.arraycopy(array, 0, result, offset, array.length);
          offset += array.length;
        }
      }
      return result;
    } else {
      int totalLength = first.length;
      for (byte[] array : rest) {
        if (array != null) {
          totalLength += array.length;
        }
      }
      byte[] result = Arrays.copyOf(first, totalLength);
      int offset = first.length;
      for (byte[] array : rest) {
        if (array != null) {
          System.arraycopy(array, 0, result, offset, array.length);
          offset += array.length;
        }
      }
      return result;
    }
  }

  /**
   * 合并字节数组
   *
   * @param rest
   * @return
   */
  public static byte[] concatAll(List<byte[]> rest) {
    int totalLength = 0;
    for (byte[] array : rest) {
      if (array != null) {
        totalLength += array.length;
      }
    }
    byte[] result = new byte[totalLength];
    int offset = 0;
    for (byte[] array : rest) {
      if (array != null) {
        System.arraycopy(array, 0, result, offset, array.length);
        offset += array.length;
      }
    }
    return result;
  }

  public static float byte2Float(byte[] bs) {
    return Float.intBitsToFloat(
        (((bs[3] & 0xFF) << 24) + ((bs[2] & 0xFF) << 16) + ((bs[1] & 0xFF) << 8) + (bs[0] & 0xFF)));
  }

  public static float byteBE2Float(byte[] bytes) {
    int l;
    l = bytes[0];
    l &= 0xff;
    l |= ((long) bytes[1] << 8);
    l &= 0xffff;
    l |= ((long) bytes[2] << 16);
    l &= 0xffffff;
    l |= ((long) bytes[3] << 24);
    return Float.intBitsToFloat(l);
  }

  public static int cbc(byte[] bs, int start, int end) {
    if (start < 0 || end > bs.length)
      throw new ArrayIndexOutOfBoundsException(
          "bcc error : index out of bounds(start="
              + start
              + ",end="
              + end
              + ",bytes length="
              + bs.length
              + ")");
    int cs = 0;
    for (int i = start; i < end; i++) {
      cs ^= bs[i];
    }
    return cs;
  }

  public static int getBitRange(int number, int start, int end) {
    if (start < 0) throw new IndexOutOfBoundsException("min index is 0,but start = " + start);
    if (end >= Integer.SIZE)
      throw new IndexOutOfBoundsException(
          "max index is " + (Integer.SIZE - 1) + ",but end = " + end);

    return (number << Integer.SIZE - (end + 1)) >>> Integer.SIZE - (end - start + 1);
  }

  public static int getBitAt(int number, int index) {
    if (index < 0) throw new IndexOutOfBoundsException("min index is 0,but " + index);
    if (index >= Integer.SIZE)
      throw new IndexOutOfBoundsException("max index is " + (Integer.SIZE - 1) + ",but " + index);

    return ((1 << index) & number) >> index;
  }

  public static int getBitAtS(int number, int index) {
    String s = Integer.toBinaryString(number);
    return Integer.parseInt(s.charAt(index) + "");
  }

  @Deprecated
  public static int getBitRangeS(int number, int start, int end) {
    String s = Integer.toBinaryString(number);
    StringBuilder sb = new StringBuilder(s);
    while (sb.length() < Integer.SIZE) {
      sb.insert(0, "0");
    }
    String tmp = sb.reverse().substring(start, end + 1);
    sb = new StringBuilder(tmp);
    return Integer.parseInt(sb.reverse().toString(), 2);
  }

  public static String stringToAscii(String value) {
    StringBuffer sbu = new StringBuffer();
    char[] chars = value.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      sbu.append((int) chars[i]);
    }
    return sbu.toString();
  }

  public static String asciiToString(String value) {
    StringBuffer sbu = new StringBuffer();
    String[] chars = value.split(",");
    for (int i = 0; i < chars.length; i++) {
      sbu.append((char) Integer.parseInt(chars[i]));
    }
    return sbu.toString();
  }

  /**
   * byte to ascII
   *
   * @param b
   * @return
   */
  public static String byteToAscii(byte[] b) {
    StringBuffer sbf = new StringBuffer();
    for (int i = 0; i < b.length; i++) sbf.append((char) b[i]);
    return sbf.toString();
  }

  /**
   * Convert hex string to byte[]
   *
   * @param hexString the hex string
   * @return byte[]
   */
  public static byte[] hexStringToBytes(String hexString) {
    if (hexString == null || hexString.equals("")) {
      return null;
    }
    hexString = hexString.toUpperCase();
    int length = hexString.length() / 2;
    char[] hexChars = hexString.toCharArray();
    byte[] d = new byte[length];
    for (int i = 0; i < length; i++) {
      int pos = i * 2;
      d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
    }
    return d;
  }

  public static String bytesToHexString(byte[] src) {
    StringBuilder stringBuilder = new StringBuilder("");
    if (src == null || src.length <= 0) {
      return null;
    }
    for (int i = 0; i < src.length; i++) {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return stringBuilder.toString();
  }

  /**
   * Convert char to byte
   *
   * @param c char
   * @return byte
   */
  private static byte charToByte(char c) {
    return (byte) "0123456789ABCDEF".indexOf(c);
  }

  /**
   * 秘钥长度处理，去除秘钥加密类型 （1+32 ->32,1+16->16,16->16,32->32)
   *
   * @param oldkey
   * @return
   */
  public static byte[] keyHandle(byte[] oldkey) {
    if ((oldkey.length & 0x01) == 0) {
      return oldkey;
    }
    byte[] key = new byte[oldkey.length - 1];
    System.arraycopy(oldkey, 1, key, 0, key.length);
    return key;
  }

  /**
   * 获取末尾长度为len的数据
   *
   * @param data
   * @param len
   * @return
   */
  public static byte[] getDataEnd(byte[] data, int len) {
    if (data.length < len) {
      return data;
    }
    byte[] data0 = new byte[len];
    System.arraycopy(data, data.length - len, data0, 0, 8);
    return data0;
  }
}
