package mytest.adcptest;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;

import java.nio.charset.Charset;
import java.security.SecureRandom;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/3/7 15:13
 * @Modified By:
 */
@Controller("/")
@Secured(SecurityRule.IS_ANONYMOUS)
public class ADCPTestController {

    @Post()
    public byte[] getDUTInfo(byte[] reqData){
        byte[] procedureID = new byte[32];
        new SecureRandom().nextBytes(procedureID);

        byte[] interfaceId = InterfaceType.DeviceStatus.getInterfaceValue();
        byte[] statusCode = new byte[]{0x00};

        String rspData ="meta:a;meta:b";
        byte[] rspByte = rspData.getBytes();
        int length = rspByte.length;
        byte[] rspLenByte = BitOperator.integerTo4Bytes(length);


        byte[] bytes = BitOperator.concatAll(procedureID, interfaceId, statusCode, rspLenByte, rspByte);


        return bytes;
    }


}
