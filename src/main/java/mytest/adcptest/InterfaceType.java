package mytest.adcptest;


import java.util.HashMap;
import java.util.Map;

/**
 * @Author lys
 * @Description
 * @create 2022-3-3 14:06
 * @Modified By:
 */
public enum InterfaceType {

    DeviceStatus                            (new byte[]{(byte)0x00, (byte)0x01}),
    DeviceConfig                            (new byte[]{(byte)0x00, (byte)0x02}),
    AuthenticationInitiatorAuthTwoWay       (new byte[]{(byte)0x0a, (byte)0x01}),
    AuthenticationInitiatorAuthOneWay       (new byte[]{(byte)0x0a, (byte)0x02}),
    AuthenticationInitiatorFastAuthTwoWay   (new byte[]{(byte)0x0a, (byte)0x03}),
    AuthenticationInitiatorFastAuthOneWay   (new byte[]{(byte)0x0a, (byte)0x04}),
    AuthenticationInitiatorFastAuth2Auth    (new byte[]{(byte)0x0a, (byte)0x05}),
    AuthenticationInitiatorUpdateCRL        (new byte[]{(byte)0x0a, (byte)0x06}),
    AuthenticationInitiatorRequireCRL       (new byte[]{(byte)0x0a, (byte)0x07}),
    AuthenticationInitiatorAuth2CertError   (new byte[]{(byte)0x0a, (byte)0x08}),
    AuthenticationInitiatorAuth2MacError    (new byte[]{(byte)0x0a, (byte)0x09}),
    AuthenticationInitiatorFastAuthMacError (new byte[]{(byte)0x0a, (byte)0x0a}),
    AuthenticationResponderAuth             (new byte[]{(byte)0x0b, (byte)0x01}),
    AuthenticationResponderFastAuth         (new byte[]{(byte)0x0b, (byte)0x02}),
    AuthenticationResponderFastAuth2Auth    (new byte[]{(byte)0x0b, (byte)0x03}),
    AuthenticationResponderUpdateCRL        (new byte[]{(byte)0x0b, (byte)0x04}),
    AuthenticationResponderRequireCRL       (new byte[]{(byte)0x0b, (byte)0x05}),
    AuthenticationResponderAuth1DKPKError   (new byte[]{(byte)0x0b, (byte)0x06}),
    AuthenticationResponderAuth3MacError    (new byte[]{(byte)0x0b, (byte)0x07}),
    AuthenticationResponderAuth3CertError   (new byte[]{(byte)0x0b, (byte)0x08});

    static{
        Map<String, InterfaceType> interfaceTypeMap = new HashMap<>();
        for(InterfaceType type :InterfaceType.values()){
            interfaceTypeMap.put(BitOperator.bytesToHexString(type.getInterfaceValue()).toLowerCase(), type);
        }
        InterfaceType.setInterfaceTypeMap(interfaceTypeMap);
    }

    private static Map<String, InterfaceType> interfaceTypeMap;

    public static Map<String, InterfaceType> getInterfaceTypeMap() {
        return interfaceTypeMap;
    }

    public static void setInterfaceTypeMap(Map<String, InterfaceType> interfaceTypeMap) {
        InterfaceType.interfaceTypeMap = interfaceTypeMap;
    }

    InterfaceType(byte[] interfaceValue){
        this.interfaceValue = interfaceValue;
    }

    private byte[] interfaceValue;

    public byte[] getInterfaceValue() {
        return interfaceValue;
    }

    public void setInterfaceValue(byte[] interfaceValue) {
        this.interfaceValue = interfaceValue;
    }

}
