package mytest.game;

import org.junit.jupiter.api.Test;

import java.awt.image.SampleModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description 十万人参与，每人输入一个正整数，有且仅有一次机会，哪个数字能最高概率满足唯一且最小？
 * @create 2023/7/26 9:34
 * @Modified By:
 */
public class CalGame {

    @Test
    public void CalGame() {
        List<Integer> recordWinner = new ArrayList<>();

        // 模拟1000个赢家
        for (int i = 0; i < 10000; i++) {
            showBestSelect(recordWinner);
        }

        // 计算正态分布
        int numSamples = recordWinner.size();  // 样本数量
        double standardDeviation = 0.0;


        // 计算平均值
        int sum = 0;
        for (Integer num : recordWinner) {
            sum += num;
        }
        double mean = sum / numSamples; // 平均值

        // 计算方差
        for(double num: recordWinner) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        // 方差
        double variance = standardDeviation/numSamples;

        // 标准差 是 方差得算数平方根
        standardDeviation = Math.sqrt(standardDeviation/numSamples);
        System.out.println("样本量: " + numSamples +" 期望: " + mean + "  方差:" + variance);


        double[] samples = new double[numSamples];
        Random rand = new Random();
        for (int i = 0; i < numSamples; i++) {
            samples[i] = mean + standardDeviation * rand.nextGaussian();
        }

        // 打印正态分布数组
//        for (double num : samples) {
//            System.out.println(num);
//        }

//        countMinSelect.forEach((minValue, count) -> {
//            System.out.println("minValue: " + minValue + "  count: " + count);
//        });

    }

    private void showBestSelect(List<Integer> integers) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int loopNum = 100000;
        int limit = Integer.MAX_VALUE;
        Random random = new Random();
        for (int i = 0; i < loopNum; i++) {
            int select = random.nextInt(limit);
            Integer integer = map.get(select);
            map.put(select, integer == null ? 1 : integer + 1);
        }
        AtomicInteger minSelect = new AtomicInteger(limit);
        map.forEach((select, count) -> {
            if (count == 1) {
                minSelect.set(Math.min(minSelect.get(), select));
            }
        });

        // 允许重复
        integers.add(minSelect.get());
    }


}
