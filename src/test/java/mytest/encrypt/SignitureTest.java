package mytest.encrypt;

import io.netty.util.internal.StringUtil;
import mytest.utils.encode.rsa.RSAHelper2;
import mytest.utils.encode.signiture.SignitureInfo;
import mytest.utils.encode.signiture.SignitureUtils;
import org.junit.jupiter.api.Test;

import java.security.PublicKey;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 模拟签名的用法
 * @create 2021/6/9 16:53
 * @Modified By:
 */
public class SignitureTest {

    @Test
    void signiture() {
        // 获取签名和公钥，发送给接收方，让他去验证
        String msg = "一串很长很长的东西";

        SignitureInfo signitureInfo = SignitureUtils.getSignitureAndPublicKey(msg);


        // 接收方开始校验
        PublicKey publicKey = signitureInfo.getPublicKey();
        String signiture = signitureInfo.getSigniture();

        String decipher = RSAHelper2.decipher(signiture, publicKey, RSAHelper2.KEY_SIZE / 8);

        if (StringUtil.isNullOrEmpty(decipher)) {
            System.out.println("解密失败");
        } else {
            System.out.println("接受方解密后的消息：" + decipher);
        }


    }
}
