package mytest.encrypt;

import mytest.utils.encode.aes.AES256Utils;
import mytest.utils.encode.rsa.RSAHelper;
import mytest.utils.encode.rsa.RSAHelper2;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.logging.Logger;

/**
 * @Author dwang
 * @Company neldtv
 * @Description rsa 测试
 * @create 2021/6/9 14:12
 * @Modified By:
 */
public class RSATest {
    RSAHelper2.CustomKeyPairInfo keyPair = null;
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(RSATest.class);


    @Test
    void testRsa() {
        RSAHelper2.CustomKeyPairInfo keyPair = RSAHelper2.getKeyPair();


        // 公钥加密私钥解密
        String ciphertext = "嘤嘤嘤";

        // 用公钥加密
        byte[] srcBytes = ciphertext.getBytes();
        byte[] encodeBytes = null;

        // Cipher负责完成加密或解密工作，基于RSA
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublicKey());
            encodeBytes = cipher.doFinal(srcBytes);

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (NoSuchPaddingException e1) {
            e1.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }


        // 私钥解密
        Cipher deCipher = null;
        try {
            deCipher = Cipher.getInstance("RSA");
            deCipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivateKey());
            byte[] decBytes = null;
            decBytes = deCipher.doFinal(encodeBytes);

            String decrytStr = new String(decBytes);
            logger.info(decrytStr);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }

    }

    @Test
    void testRsa2() {
        keyPair = RSAHelper2.getKeyPair();


        // 公钥加密私钥解密
        String ciphertext = "嘤嘤嘤";
        String encipher = RSAHelper2.encipher(ciphertext, keyPair.getPublicKey(), RSAHelper2.KEY_SIZE / 8 - 11);


        // 私钥解密
        String decipher = RSAHelper2.decipher(encipher, keyPair.getPrivateKey(), RSAHelper2.KEY_SIZE / 8);


        logger.info(decipher);
    }

    @Test
    void testRsa3() {
        RSAHelper.KeyPairInfo keyPair = RSAHelper.getKeyPair();


        // 公钥加密私钥解密
        String ciphertext = "嘤嘤嘤";

        String encipher = RSAHelper.encipher(ciphertext, keyPair.getPublicKey());


        // 私钥解密
        String decipher = RSAHelper.decipher(encipher, keyPair.getPrivateKey());

        logger.info(decipher);
    }
}
