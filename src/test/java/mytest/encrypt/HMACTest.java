package mytest.encrypt;

import mytest.utils.encode.mac.HMacUtil;
import org.junit.jupiter.api.Test;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 简单实现HMAC
 * @create 2021/6/9 16:51
 * @Modified By:
 */
public class HMACTest {
    public static final String MAC_NAME = "HmacSHA512";


    @Test
    void macTest() {
        // 生成密钥
        KeyGenerator instance = null;
        try {
            instance = KeyGenerator.getInstance(MAC_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        SecretKey secretKey = instance.generateKey();
        String msg = "我想发个消息";
        String macValue = HMacUtil.getHMAC(msg, secretKey);


        // 接收方
        String macValue2 = HMacUtil.getHMAC(msg, secretKey);

        //确认是不是老哥发的


    }
}
