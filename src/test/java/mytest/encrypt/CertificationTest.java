package mytest.encrypt;

import io.netty.util.internal.StringUtil;
import mytest.utils.encode.certification.CustomCertificatInfo;
import mytest.utils.encode.certification.PKIUtils;
import mytest.utils.encode.rsa.RSAHelper2;
import mytest.utils.encode.signiture.SignitureInfo;
import mytest.utils.encode.signiture.SignitureUtils;
import org.bouncycastle.asn1.pkcs.CertificationRequestInfo;
import org.junit.jupiter.api.Test;

import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 简单模拟证书流程
 * @create 2021/6/9 17:34
 * @Modified By:
 */
public class CertificationTest {

    @Test
    void certificationTest() {

        // BOb获取密钥对
        RSAHelper2.CustomKeyPairInfo keyPair = RSAHelper2.getKeyPair();
        RSAPublicKey publicKey = keyPair.getPublicKey();

        // 公钥拿去给CA注册
        CustomCertificatInfo customCertificatInfo = PKIUtils.register(publicKey,"Bob");


        // ALICE 校验CA的签名
        Boolean authenticationResult = authenticateSigniture(customCertificatInfo);

        if (!authenticationResult) {
            return;
        }

        System.out.println("这就是Bob的公钥，可以开始通讯了");
        PublicKey boBPublicKey = customCertificatInfo.getClientPublicKey();

        // April 发消息
        String ciphertext = "明天晚上不能一起了，对不起";
        String encipher = RSAHelper2.encipher(ciphertext, boBPublicKey, RSAHelper2.KEY_SIZE / 8 - 11);


        // Bob 解密
        String decipher = RSAHelper2.decipher(encipher, keyPair.getPrivateKey(), RSAHelper2.KEY_SIZE / 8);
        System.out.println(decipher);


    }


    /**
     * 校验CA签名
     * @param customCertificatInfo
     * @return
     */
    private Boolean authenticateSigniture(CustomCertificatInfo customCertificatInfo) {
        // 接收方开始校验
        PublicKey publicKey = customCertificatInfo.getCaPublicKey();
        String signiture = customCertificatInfo.getCaSignature();

        String decipher = RSAHelper2.decipher(signiture, publicKey, RSAHelper2.KEY_SIZE / 8);

        if (StringUtil.isNullOrEmpty(decipher)) {
            System.out.println("解密失败");
            return Boolean.FALSE;
        } else {
            System.out.println("接受方解密后的消息：" + decipher);

            PublicKey clientPublicKey = customCertificatInfo.getClientPublicKey();
            byte[] publicKeybyte = clientPublicKey.getEncoded();
            String publicKeyString = java.util.Base64.getEncoder().encodeToString(publicKeybyte);

            System.out.println("bob的公钥" + publicKeyString);
            return Boolean.TRUE;
        }
    }
}
