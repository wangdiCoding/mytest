package mytest.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class FastJsonTest {

    private static final ObjectMapper sObjectMapper = new ObjectMapper();
    private final Logger sLog = LoggerFactory.getLogger(this.getClass());


    @Test
    void test() {

        BeanTest beanTest = new BeanTest();
        BeanTestList beanTestList = new BeanTestList();
        SubBeanTest subBeanTest = new SubBeanTest();
        ArrayList<String> strings = new ArrayList<>();

        strings.add("test1");
        strings.add("test2_sleep");
        strings.add("test3");
        strings.add("test4");
        strings.add("test5");
        subBeanTest.setStrList(strings);
//        beanTestList.setSubBeanTestList(List.of(subBeanTest));
//        beanTest.setData(List.of(beanTestList));

        try {
            String s = sObjectMapper.writeValueAsString(beanTest);
            sLog.info(s);


            BeanTest beanTestCopy = sObjectMapper.readValue(s, BeanTest.class);

            sLog.info(beanTestCopy.toString());


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testJson() throws JsonProcessingException {
//        "{\"颜色\":\"红色\",\"版本\":\"8GB+128GB\"}";
        String source = "{\"颜色\":\"蓝色\",\"版本\":\"8GB+128GB\"},{\"颜色\":\"蓝色\",\"版本\":\"6GB+64GB\"},{\"颜色\":\"红色\",\"版本\":\"8GB+128GB\"},{\"颜色\":\"红色\",\"版本\":\"6GB+64GB\"}";
        StringBuilder stringBuilder = new StringBuilder("[");
        stringBuilder.append(source);
        StringBuilder target = stringBuilder.append("]");
//        ArrayList<Map<String, String>> listMap = new ArrayList<>();
//        listMap = sObjectMapper.readValue(target.toString(), listMap.getClass());

        ArrayList<String> stringsMap = new ArrayList<>();
        stringsMap.add("{\"颜色\":\"蓝色\",\"版本\":\"8GB+128GB\"}");
        stringsMap.add("{\"颜色\":\"蓝色\",\"版本\":\"6GB+64GB\"}");
        stringsMap.add("{\"颜色\":\"红色\",\"版本\":\"8GB+128GB\"}");
        stringsMap.add("{\"颜色\":\"红色\",\"版本\":\"6GB+128GB\"}");

        HashMap<String, Set> spuSpecItems = new HashMap<>();
        for (String s : stringsMap) {
            addSpuSpec(spuSpecItems,s) ;
        }
//        HashMap<String, String> map = new HashMap<>();
//        map.put("颜色","蓝色");
//        map.put("版本","8GB+128GB");
//        HashMap<String, String> map2 = new HashMap<>();
//        map2.put("颜色","蓝色");
//        map2.put("版本","6GB+64GB");
//        listMap.add(map);
//        listMap.add(map2);
//
//        String s = sObjectMapper.writeValueAsString(listMap);
//
//        System.out.println(s);
//        ArrayList arrayList = sObjectMapper.convertValue(source, listMap.getClass());


    }

    private void addSpuSpec(HashMap<String, Set> spuSpecItems, String spec) {
        Map<String, String> specMap = new HashMap<>();
        try {
            specMap = sObjectMapper.readValue(spec, specMap.getClass());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Set<String> keySet = specMap.keySet();
        for (String key : keySet) {
            Set set = spuSpecItems.get(key);
            if (set == null) {
                set = new HashSet();
                set.add(specMap.get(key));
                spuSpecItems.put(key, set);
            } else {
                set.add(specMap.get(key));
            }
        }
    }
}
