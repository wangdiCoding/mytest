package mytest.tls;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/7/21 16:42
 * @Modified By:
 */
public class MyHostnameVerifier implements HostnameVerifier {
    @Override
    public boolean verify(String hostname, SSLSession sslSession) {
        System.out.println("hostname： " + hostname);
        if ("localhost".equals(hostname)) {
            return true;
        }
        return false;
    }
}
