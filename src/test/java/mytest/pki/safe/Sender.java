package mytest.pki.safe;

import mytest.utils.encode.EncrypyUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 发送方代码
 * @create 2021/7/2 14:40
 * @Modified By:
 */
public class Sender {
//    public static final int BufferSize = 8192;
    static String outPath = "E:\\out.txt";
    static char[] passWd = "123456".toCharArray();
    static ClassLoader classLoader = Sender.class.getClassLoader();

    @BeforeEach
    void init() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    void sender() throws Exception {

        // 加载自己的keystore
        InputStream resourceAsStream = classLoader.getResourceAsStream("sender/sender.keystore");
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(resourceAsStream, passWd);
        resourceAsStream.close();


        // 生成随机密钥，用做会话密钥
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256);
        SecretKey secretKey = keyGen.generateKey();


        byte[] keyEncoded = secretKey.getEncoded();

        // 读取接受者的证书
        InputStream receiverIn = classLoader.getResourceAsStream("receiver/receiver.cer");
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) cf.generateCertificate(receiverIn);

        receiverIn.close();

        // 证书的公钥可以用来加密会话密钥（混合加密）
        Cipher keyCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        keyCipher.init(Cipher.ENCRYPT_MODE, certificate);
        byte[] keyEncrypt = keyCipher.doFinal(keyEncoded);


        // 自己的密钥库中拿出私钥对内容进行签名
        PrivateKey senderPrivateKey = (PrivateKey) keyStore.getKey("sender", passWd);
        Signature signature = Signature.getInstance("MD5ANDSHA1WITHRSA");
        signature.initSign(senderPrivateKey);
        byte[] bytes = new byte[EncrypyUtil.kBufferSize];

        InputStream sendFileInputStream = classLoader.getResourceAsStream("sender/sendFile.txt");
        int len;
        while ((len = sendFileInputStream.read(bytes)) != -1) {
            signature.update(bytes, 0, len);
        }
        // 生成签名
        byte[] signResult = signature.sign();
        sendFileInputStream.close();


        // 写入到输出文件out.txt
        FileOutputStream fileOutputStream = new FileOutputStream(outPath);
        // 先写入签名
        int signLen = signResult.length;
        byte[] blen = new byte[4];
        blen[0] = (byte) (signLen & 0xff);
        blen[1] = (byte) (signLen >> 8 & 0xff);
        blen[2] = (byte) (signLen >> 16 & 0xff);
        blen[3] = (byte) (signLen >> 24 & 0xff);
        fileOutputStream.write(blen);
        fileOutputStream.write(signResult);


        // 写入会话密钥密文长度，和密钥的密文内容
        int keyEncryptLen = keyEncrypt.length;
        blen[0] = (byte) (keyEncryptLen & 0xff);
        blen[1] = (byte) (keyEncryptLen >> 8 & 0xff);
        blen[2] = (byte) (keyEncryptLen >> 16 & 0xff);
        blen[3] = (byte) (keyEncryptLen >> 24 & 0xff);
        fileOutputStream.write(blen);
        fileOutputStream.write(keyEncrypt);


        // 最后写入加密的内容：
        // 会话密钥对原文进行加密
//        Cipher contentCipher = Cipher.getInstance("AES/CFB/PKCS7Padding");


        Cipher contentCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        contentCipher.init(Cipher.ENCRYPT_MODE, secretKey, EncrypyUtil.getIV("123456"));

        // 加密輸出
        byte[] bytes2 = new byte[EncrypyUtil.kBufferSize];
        byte[] obuffer = null;
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("sender/sendFile.txt");
//        FileOutputStream fos = new FileOutputStream(EncrypyUtil.outPath);

        // 自己原生实现
        while ((len = in.read(bytes2)) != -1) {
            obuffer = contentCipher.update(bytes2, 0, len);
            if (obuffer != null) {
                fileOutputStream.write(obuffer);
                obuffer = null;
            }
        }
        obuffer = contentCipher.doFinal();
        if (obuffer != null) {
            fileOutputStream.write(obuffer);
            obuffer = null;
        }
        fileOutputStream.flush();

        in.close();
        // 关闭要在ciperOutputstream之后
        fileOutputStream.close();


        // 写了别的东西不能直接使用cipherOutputStream?


        // 但是为什么签名的时候不顺便把数据也加密了？
        // 例子就很离谱
/*
        byte[] cipherbuffer = null;
        InputStream sendFileInputStream2 = classLoader.getResourceAsStream("sender/sendFile.txt");
        while ((len = sendFileInputStream2.read(bytes)) != -1) {
            cipherbuffer = contentCipher.update(bytes);
            fileOutputStream.write(cipherbuffer, 0, len);
        }
        cipherbuffer = contentCipher.doFinal();
        fileOutputStream.write(cipherbuffer);


        sendFileInputStream2.close();
        fileOutputStream.close();*/
    }


    @Test
    void showOut() throws IOException {

        FileInputStream fileInputStream = new FileInputStream(outPath);

        byte[] bytes = new byte[EncrypyUtil.kBufferSize];
        int len;
        while ((len = fileInputStream.read(bytes)) != -1) {
            printByte(bytes);
        }

        fileInputStream.close();

    }




    private void printByte(byte[] bytes) {
        for (byte aByte : bytes) {
            System.out.print(aByte);
        }
    }




    private static IvParameterSpec getIV(String input) {
        SecureRandom secureRandom = new SecureRandom(input.getBytes());
        byte[] IV = new byte[16];
        secureRandom.nextBytes(IV);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(IV);
        return ivParameterSpec;
    }

}
