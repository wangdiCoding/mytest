package mytest.pki.safe;

import mytest.pki.FileEncDec;
import mytest.utils.encode.EncrypyUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/7/5 16:47
 * @Modified By:
 */
public class TestSafe {
    public static final String INIT = "AES/CBC/PKCS7Padding";

    @BeforeEach
    void init() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * 测试问题出现原因是什么
     * 读取文档加密，然后再解密
     */
    @Test
    void test2() throws Exception {
        String input = "123456";

        // 生成key
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        SecureRandom secureRandom = new SecureRandom(input.getBytes());
        keyGen.init(secureRandom);
        SecretKey secretKey = keyGen.generateKey();


        // 生成cipher
        Cipher contentCipher = Cipher.getInstance(INIT);
        contentCipher.init(Cipher.ENCRYPT_MODE, secretKey, EncrypyUtil.getIV(input));


        // 加密輸出
        byte[] bytes = new byte[EncrypyUtil.kBufferSize];
        byte[] obuffer = null;
        int len;
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("sender/sendFile.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(EncrypyUtil.outPath);

        // 自己原生实现
        while ((len = in.read(bytes)) != -1) {
            obuffer = contentCipher.update(bytes, 0, len);
            if (obuffer != null) {
                fileOutputStream.write(obuffer);
                obuffer = null;
            }
        }
        obuffer = contentCipher.doFinal();
        if (obuffer != null) {
            fileOutputStream.write(obuffer);
            obuffer = null;
        }
        fileOutputStream.flush();

        in.close();
        // 关闭要在ciperOutputstream之后
        fileOutputStream.close();


//        // 文件加密输出
//       CipherOutputStream out = new CipherOutputStream(fileOutputStream, contentCipher);
//        while ((len = in.read(bytes)) != -1) {
//            out.write(bytes, 0, len);
//        }
//        in.close();
//        out.close();
//        // 关闭要在ciperOutputstream之后
//        fileOutputStream.close();



        // 解密


        // cipher
        Cipher contentCipher2 = Cipher.getInstance(INIT);
        contentCipher2.init(Cipher.DECRYPT_MODE, secretKey, EncrypyUtil.getIV("123456"));

        // 文件加密输出
        FileInputStream fin = new FileInputStream(EncrypyUtil.outPath);
        FileOutputStream fileOutputStream2 = new FileOutputStream(EncrypyUtil.outPath2);
        byte[] encryptContent = new byte[EncrypyUtil.kBufferSize];
        byte[] obuffer2 = null;
        int len2;


        // 自己原生实现
        while ((len2 = fin.read(encryptContent)) != -1) {
            obuffer2 = contentCipher2.update(encryptContent, 0, len2);
            if (obuffer2 != null) {
                fileOutputStream2.write(obuffer2);
                obuffer2 = null;
            }
        }
        obuffer2 = contentCipher2.doFinal();
        if (obuffer2 != null) {
            fileOutputStream2.write(obuffer2);
            obuffer2 = null;
        }
        fileOutputStream2.flush();

        fin.close();
        // 关闭要在ciperOutputstream之后
        fileOutputStream2.close();


//        CipherOutputStream out2 = new CipherOutputStream(fileOutputStream2, contentCipher2);
//        while ((len2 = fin.read(encryptContent)) != -1) {
//            out2.write(encryptContent, 0, len2);
//        }
//        fin.close();
//        out2.close();
//
//        // 关闭要在ciperOutputstream之后
//        fileOutputStream2.close();


    }


    /**
     * 功能描述 拿之前的例子，直接拿來加解密文件
     *
     * @param
     * @return void
     * @author wangdi
     * @date 2021/7/5 21:02
     */
    @Test
    void test3() throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IOException,
            BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {

        FileEncDec.handle("123456", EncrypyUtil.inPath, EncrypyUtil.outPath, "e");
        FileEncDec.handle("123456", EncrypyUtil.outPath, EncrypyUtil.outPath2, "d");


    }


}
