package mytest.pki.safe;

import mytest.utils.encode.EncrypyUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 发送方代码
 * @create 2021/7/2 14:40
 * @Modified By:
 */
public class Receiver {
    static String outPath = "E:\\out.txt";
    static String outPath2 = "E:\\out2.txt";
    static char[] passWd = "123456".toCharArray();
    static ClassLoader classLoader = Receiver.class.getClassLoader();


    @BeforeEach
    void init() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    void receiver() throws Exception {

        // 加载自己的keystore
        InputStream resourceAsStream = classLoader.getResourceAsStream("receiver/receiver.keystore");
        char[] passWd = "123456".toCharArray();
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(resourceAsStream, passWd);
        resourceAsStream.close();


        // 加载发送者的证书
        InputStream senderIn = classLoader.getResourceAsStream("sender/sender.cer");
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) cf.generateCertificate(senderIn);
        senderIn.close();


        // 读取文件
        int len;
        byte[] blen = new byte[4];
        FileInputStream fin = new FileInputStream(outPath);
        fin.read(blen);

        len = (blen[0] & 0xff) | ((blen[1] << 8) & 0xff00) | ((blen[2] << 24) >>> 8) | (blen[3] << 24);
        byte[] signBuffer = new byte[len];
        // 读取签名
        fin.read(signBuffer);




        // 读取密钥
        fin.read(blen);
        len = (blen[0] & 0xff) | ((blen[1] << 8) & 0xff00) | ((blen[2] << 24) >>> 8) | (blen[3] << 24);
        byte[] keyBuffer = new byte[len];
        fin.read(keyBuffer);





        // 自己库的私钥解密会话密钥
        PrivateKey receiverPrivateKey = (PrivateKey) keyStore.getKey("receiver", passWd);
        Cipher keyCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        keyCipher.init(Cipher.DECRYPT_MODE, receiverPrivateKey);
        byte[] keyencode = keyCipher.doFinal(keyBuffer);

        // 解密会话内容

        Cipher contentCipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec secretKey = new SecretKeySpec(keyencode, "AES");
        contentCipher2.init(Cipher.DECRYPT_MODE, secretKey, EncrypyUtil.getIV("123456"));


        FileOutputStream fileOutputStream2 = new FileOutputStream(EncrypyUtil.outPath2);
        byte[] encryptContent = new byte[EncrypyUtil.kBufferSize];
        byte[] obuffer2 = null;
        int len2;


        // 自己原生实现
        while ((len2 = fin.read(encryptContent)) != -1) {
            obuffer2 = contentCipher2.update(encryptContent, 0, len2);
            if (obuffer2 != null) {
                fileOutputStream2.write(obuffer2);
                obuffer2 = null;
            }
        }
        obuffer2 = contentCipher2.doFinal();
        if (obuffer2 != null) {
            fileOutputStream2.write(obuffer2);
            obuffer2 = null;
        }
        fileOutputStream2.flush();

        fin.close();
        // 关闭要在ciperOutputstream之后
        fileOutputStream2.close();

        // 输出明文之后才能证明，明文是否被串改
        boolean verifySigResult = verifySig(signBuffer,certificate);





    }

    /**
     * 验证签名，公钥验证
     * @param signBuffer
     * @param certificate
     * @return
     */
    private boolean verifySig(byte[] signBuffer, X509Certificate certificate) {
        Signature signature = null;
        int len;
        byte[] bytes = new byte[EncrypyUtil.kBufferSize];
        try {

            signature = Signature.getInstance("MD5ANDSHA1WITHRSA");
            signature.initVerify(certificate);
            FileInputStream fin = new FileInputStream(EncrypyUtil.outPath2);

            while ((len = fin.read(bytes)) != -1) {
              signature.update(bytes,0,len);
            }
            fin.close();


            boolean verify = signature.verify(signBuffer);
            return verify;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return false;

    }


    @Test
    void showOut() throws IOException {

        FileInputStream fileInputStream = new FileInputStream(outPath);

        byte[] bytes = new byte[EncrypyUtil.kBufferSize];
        int len;
        while ((len = fileInputStream.read(bytes)) != -1) {
            printByte(bytes);
        }

        fileInputStream.close();

    }

    private void printByte(byte[] bytes) {
        for (byte aByte : bytes) {
            System.out.print(aByte);
        }
    }

    private static IvParameterSpec getIV(String input) {
        SecureRandom secureRandom = new SecureRandom(input.getBytes());
        byte[] IV = new byte[16];
        secureRandom.nextBytes(IV);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(IV);
        return ivParameterSpec;
    }
}
