package mytest.pki;

import org.junit.jupiter.api.Test;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/7/2 11:30
 * @Modified By:
 */
public class KeyStoreTest {

    @Test
    void keystore() {

        KeyStore jks = null;
        try {
            jks = KeyStore.getInstance("JKS");
            Certificate certificate = jks.getCertificate("mykey");

        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }
}
