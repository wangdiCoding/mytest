package mytest.pki;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.*;
import java.util.Scanner;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 模拟加解密
 * @create 2021/7/1 17:46
 * @Modified By:
 */
public class FileEncDec {

    public static final int kBufferSize = 8192;
    public static IvParameterSpec ivParameterSpec = null;
    public static Scanner scan = new Scanner(System.in);

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException {

//        String input = getScanInput("请输入您的口令：");
//        String inputpath = getScanInput("请输入您的文件路径：");
//        String outputpath = getScanInput("请输入您的文件输出路径：");
//        String handle = getScanInput("请输入您是需要加密还是解密输入:e/d");

        String input = "123456";

//        String inputpath = "E:\\a.txt";
//        String outputpath = "E:\\b.txt";
//        String handle = "e";

        String inputpath = "E:\\b.txt";
        String outputpath = "E:\\c.txt";
        String handle = "d";

        SecureRandom secureRandom = new SecureRandom(input.getBytes());


        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance("AES/CFB/PKCS7Padding");

        if ("e".equals(handle)) {

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, getIV(secureRandom));
        } else if ("d".equals(handle)) {
            cipher.init(Cipher.DECRYPT_MODE, secretKey, getIV(secureRandom));
        } else {
            return;
        }


        FileInputStream in = new FileInputStream(inputpath);
        FileOutputStream fileOutputStream = new FileOutputStream(outputpath);

        // 文件加密输出
        CipherOutputStream out = new CipherOutputStream(fileOutputStream, cipher);

        byte[] bytes = new byte[kBufferSize];
        int len;
        while ((len = in.read(bytes)) != -1) {
            out.write(bytes, 0, len);
        }


        in.close();
        out.close();

        // 关闭要在ciperOutputstream之后
        fileOutputStream.close();
        scan.close();
    }

    public static void handle(String input, String inputpath, String outputpath, String handle) throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidAlgorithmParameterException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException {

        SecureRandom secureRandom = new SecureRandom(input.getBytes());


        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance("AES/CFB/PKCS7Padding");

        if ("e".equals(handle)) {

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, getIV(secureRandom));
        } else if ("d".equals(handle)) {
            cipher.init(Cipher.DECRYPT_MODE, secretKey, getIV(secureRandom));
        } else {
            return;
        }


        FileInputStream in = new FileInputStream(inputpath);
        FileOutputStream fileOutputStream = new FileOutputStream(outputpath);

        // 文件加密输出
        CipherOutputStream out = new CipherOutputStream(fileOutputStream, cipher);

        byte[] bytes = new byte[kBufferSize];
        int len;
        while ((len = in.read(bytes)) != -1) {
            out.write(bytes, 0, len);
        }


        in.close();
        out.close();

        // 关闭要在ciperOutputstream之后
        fileOutputStream.close();
        scan.close();
    }

    private static String getScanInput(String tips) {
        String input = "";
        System.out.println(tips);
        if (scan.hasNext()) {
            input = scan.next();
            System.out.println("输入的数据为：" + input);
        }
        return input;
    }


    private static IvParameterSpec getIV(SecureRandom secureRandom) {
//        if (ivParameterSpec != null) {
//            return ivParameterSpec;
//        }
        byte[] IV = new byte[16];
//        secureRandom.nextBytes(IV);
        ivParameterSpec = new IvParameterSpec(IV);
        return ivParameterSpec;
    }
}
