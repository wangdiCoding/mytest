package mytest.pki;

import mytest.utils.PrintUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/6/24 18:21
 * @Modified By:
 */
public class CipherTest {

    @BeforeEach
    void before() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    void show() {
        Set<String> keyGenerator = Security.getAlgorithms("KeyGenerator");
        PrintUtils.printList(keyGenerator, "KeyGenerator");


        Set<String> cipher = Security.getAlgorithms("Cipher");
        cipher = cipher.stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
        PrintUtils.printList(cipher, "Cipher");
    }


    @Test
    void demo1() {
        String source = "Hello java";
        KeyGenerator keyGenerator = null;
        SecureRandom secureRandom = new SecureRandom();
        byte[] IV = new byte[16];
        secureRandom.nextBytes(IV);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(IV);

        try {
            keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            Key secretKey = keyGenerator.generateKey();

            Cipher cipher = Cipher.getInstance("AES/CFB/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
            byte[] encrypData = cipher.doFinal(source.getBytes("UTF-8"));

            // 一样的算法，一样的初始向量才能解密
            Cipher cipher2 = Cipher.getInstance("AES/CFB/PKCS7Padding");
            cipher2.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
            byte[] bytes = cipher2.doFinal(encrypData);

            String s = new String(bytes, "UTF-8");
            System.out.println(s);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }
}
