package mytest.pki;

import mytest.utils.PrintUtils;
import org.junit.jupiter.api.Test;

import javax.crypto.KeyGenerator;
import java.security.*;
import java.util.Set;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 签名
 * @create 2021/6/25 11:10
 * @Modified By:
 */
public class SignatureDemo {

    @Test
    void show() {

        Set<String> signature = Security.getAlgorithms("Signature");
        PrintUtils.printList(signature, "Signature");


    }

    @Test
    void demo() throws Exception{
        byte[] plainTest = "signature java".getBytes();

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair key = keyGen.generateKeyPair();
        Signature signature = Signature.getInstance("MD5ANDSHA1WITHRSA");
        signature.initSign(key.getPrivate());

        signature.update(plainTest);

        byte[] signResult = signature.sign();



        // 开始验证签名
        Signature signature2 = Signature.getInstance("MD5ANDSHA1WITHRSA");
        signature2.initVerify(key.getPublic());
        signature2.update(plainTest);

        boolean verify = signature2.verify(signResult);
    }


}
