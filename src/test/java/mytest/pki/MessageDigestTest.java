package mytest.pki;

import mytest.utils.encode.EncrypyUtil;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Set;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/6/24 17:39
 * @Modified By:
 */
public class MessageDigestTest {

    /**
     * md算法展示
     */
    @Test
    void show() {
        Set<String> messageDigest = Security.getAlgorithms("MessageDigest");

        for (String s : messageDigest) {
            System.out.println(s);
        }
    }

    @Test
    void test() {

        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-512");
            String msgtoDigest  = "hello message digest";
            instance.update(msgtoDigest.getBytes("UTF-8"));
            instance.update(msgtoDigest.getBytes("UTF-8"));
            instance.update(msgtoDigest.getBytes("UTF-8"));
            instance.update(msgtoDigest.getBytes("UTF-8"));
            byte[] digest = instance.digest();

            String s = EncrypyUtil.byte2Hex(digest);
            System.out.println(s);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
