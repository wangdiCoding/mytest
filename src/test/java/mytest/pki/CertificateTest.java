package mytest.pki;

import mytest.utils.encode.EncrypyUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 证书测试demo
 * @create 2021/6/25 11:28
 * @Modified By:
 */
public class CertificateTest {
    public static final int kBufferSize = 8192;

    private static Logger logger = LoggerFactory.getLogger(CertificateTest.class);

    @Test
    void test() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("my.cer");


        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) cf.generateCertificate(resourceAsStream);


            logger.info(String.valueOf(certificate.getVersion()));
            logger.info(String.valueOf(certificate.getSerialNumber()));
            logger.info(String.valueOf(certificate.getNotBefore()));
            logger.info(String.valueOf(certificate.getNotAfter()));
            logger.info(String.valueOf(certificate.getSubjectDN().getName()));
            logger.info(String.valueOf(certificate.getIssuerDN().getName()));

            logger.info(certificate.getSigAlgName());
            byte[] signature = certificate.getSignature();
            String s = EncrypyUtil.byte2Hex(signature);
            logger.info(s);


        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }





}
