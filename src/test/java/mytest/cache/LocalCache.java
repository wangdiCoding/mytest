package mytest.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author dwang
 * @Description 缓存
 * @create 2023/3/13 14:57
 * @Modified By:
 */
public class LocalCache {

    private static Map<String, String> kmMap = new ConcurrentHashMap<>();


    private static Map<String, Long> orderMap = new ConcurrentHashMap<>();
    private static Map<String, Long> startTimeMap = new ConcurrentHashMap<>();


    public <V> V putIfAbsent(CacheKey cacheKey, String field, V current) {
        switch (cacheKey) {
            case KM_ORDER:
                Long value = (Long) current;
                return (V) orderMap.putIfAbsent(field, value);

            case KM_START_TIME:
                Long startTimeValue = (Long) current;
                return (V) startTimeMap.putIfAbsent(field, startTimeValue);

            default:
                return null;
        }
    }


    public void remove(CacheKey key, String field) {
        switch (key) {
            case KM_ORDER:
                orderMap.remove(field);

            case KM_START_TIME:
                startTimeMap.remove(field);
        }
    }

    public <V> V get(CacheKey key, String field) {
        switch (key) {
            case KM_ORDER:
                return (V) orderMap.get(field);

            case KM_START_TIME:
                return (V) startTimeMap.get(field);
        }
        return null;
    }
}
