package mytest.cache;

/**
 * @Author dwang
 * @Description 两个不同的场景
 * @create 2023/3/13 15:33
 * @Modified By:
 */
public enum CacheKey {
    CA,KM,

    /**
     * km的两个MAP
     */
    KM_ORDER,
    KM_START_TIME,

}
