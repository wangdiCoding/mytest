package mytest.cache;

import org.junit.jupiter.api.Test;

/**
 * @Author dwang
 * @Description 缓存测试
 * @create 2023/3/13 17:09
 * @Modified By:
 */
public class CacheTest {

    @Test
    public void test_cache() {

        LocalCache localCache = new LocalCache();
        String orderId = "orderId";
        long current = 666;
        Long aLong = localCache.putIfAbsent(CacheKey.KM_ORDER, orderId, current);

        long current2 = 999;
        Long aLong2 = localCache.putIfAbsent(CacheKey.KM_ORDER, orderId, current2);


        long time = localCache.get(CacheKey.KM_ORDER, orderId);
        System.out.println(time);

    }
}
