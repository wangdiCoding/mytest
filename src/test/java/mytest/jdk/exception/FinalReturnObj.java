package mytest.jdk.exception;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/3/9 16:44
 * @Modified By:
 */
public class FinalReturnObj {
    private String name;

    public FinalReturnObj(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FinalReturnObj{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
