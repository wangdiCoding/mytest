package mytest.jdk.exception;

/**
 * @Description notify wait 在synchronized环境调用
 * @ClassName IllegalMonitorStateExceptionTest
 * @Author wangDi
 * @date 2021-05-25 18:19
 */
public class IllegalMonitorStateExceptionTest2 extends Thread {
    private volatile static Integer noteNum = 0;

    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println("要执行跑了, thread = " + threadName);
        try {
            getNote();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        addNote();
    }

    private synchronized void addNote() {
        String threadName = Thread.currentThread().getName();
        noteNum++;
        System.out.println("noteNum 加了1, thread = " + threadName);
        this.notifyAll();
    }

    private synchronized void getNote() throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        while (noteNum == 0) {
            System.out.println("我停住了, thread = " + threadName);
            this.wait();
        }
        System.out.println("我开始了, thread = " + threadName);
    }

    public static void main(String[] args) throws Exception {
        IllegalMonitorStateExceptionTest2 s1 = new IllegalMonitorStateExceptionTest2();
        IllegalMonitorStateExceptionTest2 s2 = new IllegalMonitorStateExceptionTest2();
        IllegalMonitorStateExceptionTest2 s3 = new IllegalMonitorStateExceptionTest2();
        s1.start();
//        s2.start();
        // 唤醒其他线程
        sleep(1000);
        s1.addNote();
    }
}
