package mytest.jdk.exception;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author dwang
 * @Description catch中 return和 finnally是否会覆盖
 * @create 2022/3/9 16:17
 * @Modified By:
 */
public class CatchFinallyReturnTest2 {


    /**
     * 基本数据类型查看是否会被覆盖
     */
    @Test
    public void demo1() {

        int i = getFinalBaseRtn();
        System.out.println(i);

    }

    private int getFinalBaseRtn() {
        int i = 369;
        try {
            int i1 = i / 0;
            return i;
        } catch (Exception e) {

            return i;
        } finally {
            i = 666;
            return i;
        }
    }

    @Test
    public void demo3() {
        FinalReturnObj finalRefRtn2 = getFinalRefRtn2();
        System.out.println(finalRefRtn2);
    }

    private FinalReturnObj getFinalRefRtn2() {
        FinalReturnObj finalReturnObj = new FinalReturnObj("369");
        try {
            finalReturnObj.setName("try");
            int i = 1 / 0;
            return finalReturnObj;
        } catch (Exception e) {
            e.printStackTrace();
            finalReturnObj.setName("catch");
            return finalReturnObj;
        } finally {
            finalReturnObj.setName("finally");
            return finalReturnObj;
        }
    }
}
