package mytest.jdk.exception;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author dwang
 * @Description try return和 finnally是否会覆盖
 * @create 2022/3/9 16:17
 * @Modified By:
 */
public class FinallyReturnTest1 {


    /**
     * 基本数据类型查看是否会被覆盖
     */
    @Test
    public void demo1() {

        int i = getFinalBaseRtn();
        System.out.println(i);

    }

    private int getFinalBaseRtn() {
        int i = 369;
        try {
            return i;
        } finally {
            i = 666;
            return i;
        }
    }

    /**
     * 引用数据类型 - 包装类型 - 结果一致
     */
    @Test
    public void demo2() {
        Integer i = getFinalRefRtn();
        System.out.println(i);
    }

    private int getFinalRefRtn() {
        Integer i = 369;
        try {

            return i;
        } catch (Exception e){
            return i;

        } finally {
            i = 666;
            return i;
        }
    }

    @Test
    public void demo3() {
        Map<String, String> map = getFinalRefRtn2();
        System.out.println(map);
    }

    private Map<String, String> getFinalRefRtn2() {
        HashMap<String, String> map = new HashMap<>();
        try {
            map.put("name", "369");
            return map;
        } finally {
            map.put("name", "knight");
            return map;
        }
    }
}
