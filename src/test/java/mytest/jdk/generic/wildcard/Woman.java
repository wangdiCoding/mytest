package mytest.jdk.generic.wildcard;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/1/19 14:31
 * @Modified By:
 */
public class Woman extends Person {


    private String lipstick;

    public Woman(String name, Integer age, String lipstick) {
        super(name, age);
        this.lipstick = lipstick;
    }

    public Woman(String lipstick) {
        this.lipstick = lipstick;
    }
}
