package mytest.jdk.generic.wildcard;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/1/19 14:31
 * @Modified By:
 */
public class Man extends Person {

    private String watch;
    private String car;


    public Man(String name, Integer age, String watch, String car) {
        super(name, age);
        this.watch = watch;
        this.car = car;
    }


}
