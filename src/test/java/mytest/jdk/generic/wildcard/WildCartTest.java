package mytest.jdk.generic.wildcard;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

/**
 * @Author dwang
 * @Description 通配符的测试
 * @create 2022/1/19 14:21
 * @Modified By:
 */
public class WildCartTest {


    /**
     * 测试 无限制通配符 ？
     */
    @Test
    public void test1() {
        HashSet<String> strings = new HashSet<>();
        HashSet<?> objects = strings;

        strings.add("123");
        objects.add(null);
    }




}
