package mytest.jdk.generic.wildcard;

import org.checkerframework.checker.nullness.qual.AssertNonNullIfNonNull;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author dwang
 * @Description 探究子类型化
 * @create 2022/1/19 14:35
 * @Modified By:
 */
public class Programmer<T extends Person> {

    public <T extends Person> MyPair<T> youngOld(T[] a) {

        T min = a[0];
        T max = a[1];

        for (int i = 0; i < a.length; i++) {
            if (min.getAge().compareTo(a[i].getAge()) > 0) {
                min = a[i];
            }
            if (max.getAge().compareTo(a[i].getAge()) < 0) {
                max = a[i];
            }

        }
        return new MyPair<>(min, max);
    }

    public void count(Collection<?> persons) {
        System.out.println(persons.size());
    }


    @Test
    public void countTest() {

        Programmer<Person> personProgrammer = new Programmer<>();
//        Programmer<Man> personProgrammer = new Programmer<>();

        // 违反子类型化原则，编译报错
        // 子类型规则，即任何参数化的类型是原生态类型的一个子类型，
        // Programmer<T>是Programmer<Parson>类型的一个子类型，而不是Programmer<Object>的子类型
        // public class Programmer<T extends Person> {
        List<Man> listMan = new ArrayList<>();
        Collection<Man> collectionMan = new ArrayList<>();
        List<Person> listPersion = new ArrayList<>();
        Collection<Person> collectionPersion = new ArrayList<>();

        personProgrammer.count(listMan);
        personProgrammer.count(collectionMan);
        personProgrammer.count(listPersion);
        personProgrammer.count(collectionPersion);



        // assuming that such subtyping was possible
        List<Number> list = new ArrayList<>();
        // the next line would cause a ClassCastException
        // because Double is no subtype of Integer
        list.add(new Double(.1d));


        Person[] people = new Person[4];
        people[0] = new Man("wd",123,"手环","美团");
        people[1] = new Man("wd",16,"手环","美团");
        people[2] = new Woman("wd",19,"手环");
        people[3] = new Woman("wd",17,"手环");

        personProgrammer.youngOld(people);


        Man[] mans = new Man[4];
        mans[0] = new Man("wd",123,"手环","美团");
        mans[1] = new Man("wd",123,"手环","美团");
        mans[2] = new Man("wd",123,"手环","美团");
        mans[3] = new Man("wd",123,"手环","美团");
        personProgrammer.youngOld(mans);




    }

}
