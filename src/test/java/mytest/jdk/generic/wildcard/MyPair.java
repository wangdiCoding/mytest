package mytest.jdk.generic.wildcard;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/1/19 14:36
 * @Modified By:
 */
public class MyPair<T> {
    private T first;
    private T second;

    public MyPair() {
    }

    public MyPair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }
}
