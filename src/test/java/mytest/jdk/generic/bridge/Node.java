package mytest.jdk.generic.bridge;

/**
 * @Author dwang
 * @Description 演示泛型的桥接
 * @create 2022/1/19 13:57
 * @Modified By:
 */
public class Node<T> {

    public T data;

    public Node(T data) {
        System.out.println("node setData");
        this.data = data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static class MyNode extends Node<Integer> {
        public MyNode(Integer data) {
            super(data);
        }


        @Override
        public void setData(Integer data) {
            System.out.println("mynode setData");
            super.setData(data);
        }


        // jdk 1.8没看到桥接的方法

    }

    public static void main(String[] args) {
        MyNode myNode = new MyNode(5);
        Node node = myNode;
        node.setData(123);
        Integer data = myNode.data;
        System.out.println(data);

    }
}
