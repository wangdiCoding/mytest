package mytest.jdk.jvm.leak;

import java.util.ArrayList;

/**
 * @program: mytest
 * @description: 内存泄漏举例
 * @author: dwang
 * @date: 2022-08-28 15:08
 **/
public class JVMLeakTest {
    
    
    /*** 
     */
    public static void main(String[] args) {


        leak_2();
    }

    /***
     * @description: 2. 当集合里面的对象属性被修改后，再调用 remove() 方法时不起作用；
     * @param
     * @return: void
     * @author: wangdi
     * @date: 2022-08-28 15:10
     */
    private static void leak_2() {
        ArrayList<String> strings = new ArrayList<>();

        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");


        String strB = strings.get(1);
        strings.set(1,"bb");

        // remove 失败？
        strings.remove(strB);

        System.out.println(strings);
    }


}
