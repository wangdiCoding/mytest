package mytest.jdk.jvm.visibility.demo1;

/**
 * @Author dwang
 * @Description 可见的变量值
 * @create 2023/1/12 9:57
 * @Modified By:
 */
public class Share {

    boolean shareValue;

    public boolean isShareValue() {
        return shareValue;
    }

    public void setShareValue(boolean shareValue) {
        this.shareValue = shareValue;
    }
}
