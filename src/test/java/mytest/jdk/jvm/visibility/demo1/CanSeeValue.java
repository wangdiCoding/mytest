package mytest.jdk.jvm.visibility.demo1;

/**
 * @Author dwang
 * @Description 能够看到值的写法
 * @create 2023/1/12 9:57
 * @Modified By:
 */
public class CanSeeValue {

    public static void main(String[] args) {

        Share share = new Share();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                share.setShareValue(true);
            }
        }).start();


        while (true) {
            boolean shareValue = share.isShareValue();
            System.out.println("");
            if (shareValue) {
                System.out.println("can see share value: " +shareValue);
                break;
            }
        }
    }
}
