package mytest.jdk.jvm.safepoint;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * https://mp.weixin.qq.com/s/T333wsXPyvOMCaEwMmKq0Q
 *
 * 加上 -XX:+PrintGCApplicationStoppedTime 就能看到进入save point的安全时间
 */
public class SimpleInliningTest {

    public static AtomicInteger num = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {
        Runnable runnable=()->{
            for (int i = 0; i < 100000000; i++) {
                num.getAndAdd(1);
                if(i%1000 == 0) {
                    try {
                        Thread.sleep(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println(Thread.currentThread().getName()+"end!");
        };

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);
        t1.start();
        t2.start();
        // 主线程会发生等待 但是需要大于1s
        Thread.sleep(1500);
        System.out.println("num = " + num);
    }
}
