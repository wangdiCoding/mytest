package mytest.jdk.jvm.safepoint;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/9/21 15:49
 * @Modified By:
 */

public class WhenWillItExit {
    public static void main(String[] argc) throws InterruptedException {

        for (long i = 0; i < 100000; i++) {
            countOdds(10);
        }

        Thread t = new Thread(() -> {
            long l = countOdds(Integer.MAX_VALUE);
            System.out.println("How Odd:" + l);
        });
        t.setDaemon(true);
        t.start();
        Thread.sleep(5000);
        System.out.println("end");

    }

    private static long countOdds(int limit) {
        long l = 0;
        for (int i = 0; i < limit; i++) {
            for (int j = 0; j < limit; j++) {
                if ((j & 1) == 1)
                    l++;
            }
        }
        return l;
    }
}