package mytest.jdk.jvm.safepoint;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @Author dwang
 * @Description -XX:+PrintGCApplicationStoppedTime 打印暂停时间
 * @create 2022/9/22 16:30
 * @Modified By:
 */
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Thread)
public class SafepointsAintFair {
    @Param({ "32768" })
    int size;

    @Param({ "100" })
    int intervalMs;

    byte[] soggyBottoms;

    public static void main(String[] args) throws Exception {
        // 使用一个单独进程执行测试，执行5遍warmup，然后执行5遍测试
        Options opt = new OptionsBuilder().include(SafepointsAintFair.class.getSimpleName())
                .forks(1)
                .warmupIterations(5)        //热身五次
                .measurementIterations(5)   //执行五秒
                .build();
        new Runner(opt).run();
    }



    @Setup
    public void bakeACake() {
        soggyBottoms = new byte[size];
    }

    @Benchmark
    @Group("run_together")
    public boolean contains1() {
        int needle = 1;
        byte[] haystack = soggyBottoms;
        return containsNeedle(needle, haystack);
    }

    private static boolean containsNeedle(int needle, byte[] haystack) {
        for (int i = 0; i < haystack.length - 3; i++) {
            if (((haystack[i] << 24) | (haystack[i + 1] << 16) | (haystack[i + 2] << 8) | haystack[i + 3]) == needle) {
                return true;
            }
        }
        return false;
    }

    @Benchmark
    @Group("run_together")
    public Object safepoint() {
        if(intervalMs == 0)
            return null;
        LockSupport.parkNanos(intervalMs * 1_000_000);
        return safepointMethod();
    }

    private Object safepointMethod() {
        System.gc();
        return new Object();
    }

    @Benchmark
    @Group("run_together")
    public void park() {
        if(intervalMs == 0)
            return;
        LockSupport.parkNanos(intervalMs * 1_000_000);
    }
}