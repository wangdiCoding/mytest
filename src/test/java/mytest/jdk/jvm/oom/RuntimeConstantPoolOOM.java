package mytest.jdk.jvm.oom;

import java.util.ArrayList;

/**
 * @Author dwang
 * @Description 运行时常量池溢出 - 复现不了1.8 字符串常量池不在元空间
 * @create 2022/2/10 16:14
 * @Modified By:
 */
public class RuntimeConstantPoolOOM {


    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        int i = 0;
        while (true) {
            list.add(String.valueOf(i++).intern());
//            System.out.println(i);
        }
    }
}
