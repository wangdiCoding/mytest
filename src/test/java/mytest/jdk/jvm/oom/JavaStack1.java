package mytest.jdk.jvm.oom;

/**
 * @Author dwang
 * @Description 测试stackoverflow
 * @create 2022/2/10 14:39
 * @Modified By:
 *  不断地调用
 *
 */
public class JavaStack1 {
    private int stackLength = 1;

    /**
     * 递归没有出口，最简单
     */
    public void stackLeak() {
        stackLength++;
        stackLeak();

    }

    public static void main(String[] args) {
        JavaStack1 javaStack1 = new JavaStack1();
        try {
            javaStack1.stackLeak();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("stack length :  " + javaStack1.stackLength);
        }
    }

}
