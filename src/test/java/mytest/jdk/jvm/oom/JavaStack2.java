package mytest.jdk.jvm.oom;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author dwang
 * @Description 测试栈溢出，创建线程过多导致不能再创建线程，出现oom。
 * 多线程的情况下, window条件下执行不出来,测出来的都是堆先OOM,测不出来
 *
 * -Xss2M
 * TLAB
 *
 * @create 2022/2/10 14:39
 * @Modified By:
 */
public class JavaStack2 {
    private volatile AtomicInteger callNum = new AtomicInteger(1);

    private void dontStop(){
//        System.out.println("start ! callNum" + callNum.incrementAndGet());
        while (true){
            try {
                Thread.sleep(100000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stackLeakByThread() {
        while (true){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    dontStop();
                }
            }).start();
        }

    }

    public static void main(String[] args) {
        JavaStack2 javaStack2 = new JavaStack2();
        javaStack2.stackLeakByThread();
    }

}
