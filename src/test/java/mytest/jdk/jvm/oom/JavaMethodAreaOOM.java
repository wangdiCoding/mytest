package mytest.jdk.jvm.oom;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.security.Key;
import java.util.Properties;

/**
 * @Author dwang
 * @Description 方法区异常，通过不断的创建代理类，生成大量的类对象把方法区撑爆
 * @create 2022/2/10 16:23
 * @Modified By:
 *
 *
 *  -XX:PermSize=10M -XX:MaxPermSize=10M
 *
 *  JDK1.8 使用
 *  -Xms20m -Xmx20m -XX:MetaspaceSize=3M -XX:MaxMetaspaceSize=9M
 */
public class JavaMethodAreaOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
        try {
            while (true) {
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(OOMObject.class);
                enhancer.setUseCache(false);
                enhancer.setCallback(new MethodInterceptor() {
                    @Override
                    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                        return proxy.invokeSuper(obj, args);
                    }
                });
                enhancer.create();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            long result = System.currentTimeMillis() - begin;
            System.out.println("run time= " + result + "ms");
        }

    }

}
