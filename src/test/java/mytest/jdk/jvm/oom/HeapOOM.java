package mytest.jdk.jvm.oom;

import java.util.ArrayList;

/**
 * @Author dwang
 * @Description 测试堆内存溢出
 * @create 2022/2/10 13:52
 * @Modified By:
 *  -Xms20m -Xmx20m -XX:+PrintGCDetails  -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=E:\
 *
 */
public class HeapOOM {

    static class OOMObject {

    }

    public static void main(String[] args) {
        ArrayList<OOMObject> oomObjects = new ArrayList<>();
        boolean flag = true;
        while (flag) {
            oomObjects.add(new OOMObject());
        }
    }
}
