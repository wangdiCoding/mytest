package mytest.jdk.jvm.decompile;

import capstone.Capstone;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @Author dwang
 * @Description 机器码转汇编
 * @create 2022/9/6 16:10
 * @Modified By:
 *
 *  capstone下载地址:https://github.com/capstone-engine/capstone/releases
 *  然后放到对应的路径
 *
 *  Instructions: (pc=0x0000000003884d2a)
 * 0x0000000003884d0a:   ed 6e 00 00 00 00 49 8b 14 d2 52 41 8b d5 4c 8b
 * 0x0000000003884d1a:    6d c8 8b c2 81 e0 00 00 10 00 0f 84 77 01 00 00
 * 0x0000000003884d2a:    48 3b 01 48 8b 45 e0 48 85 c0 0f 84 12 00 00 00
 * 0x0000000003884d3a:    48 83 40 08 01 48 83 58 08 00 48 83 c0 30 48 89
 *
 */
public class CapstoneTest {
    /**
     * 线程随机标识符
     */
    private static long addr = 0x0000000003884d2aL;

    private static String[] pc = {"0x0000000003884d0a", "0x0000000003884d1a", "0x0000000003884d2a", "0x0000000003884d3a",""};
    /**
     * 四位机器码
     */
    private static byte[][] hexCodesArr = {
            // 7f（127）以上都要强转
            {(byte) 0xed, (byte) 0x6e, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x49, (byte) 0x8b, (byte) 0x14, (byte) 0xd2, (byte) 0x52, (byte) 0x41, (byte) 0x8b, (byte) 0xd5, (byte) 0x4c, (byte) 0x8b},
            {(byte) 0x6d, (byte) 0xc8, (byte) 0x8b, (byte) 0xc2, (byte) 0x81, (byte) 0xe0, (byte) 0x00, (byte) 0x00, (byte) 0x10, (byte) 0x00, (byte) 0x0f, (byte) 0x84, (byte) 0x77, (byte) 0x01, (byte) 0x00, (byte) 0x00},
            {(byte) 0x48, (byte) 0x3b, (byte) 0x01, (byte) 0x48, (byte) 0x8b, (byte) 0x45, (byte) 0xe0, (byte) 0x48, (byte) 0x85, (byte) 0xc0, (byte) 0x0f, (byte) 0x84, (byte) 0x12, (byte) 0x00, (byte) 0x00, (byte) 0x00},
            {(byte) 0x48, (byte) 0x83, (byte) 0x40, (byte) 0x08, (byte) 0x01, (byte) 0x48, (byte) 0x83, (byte) 0x58, (byte) 0x08, (byte) 0x00, (byte) 0x48, (byte) 0x83, (byte) 0xc0, (byte) 0x30, (byte) 0x48, (byte) 0x89}


    };

    public static void main(String[] args) {
        // 设置存放capstone.dll库的路径,需要注意跟jar包的版本保持一致
        System.setProperty("jna.library.path", "F:\\tool\\decompile\\capstone-3.0.5-rc2-win64");

        Capstone capstone = new Capstone(Capstone.CS_ARCH_X86, Capstone.CS_MODE_64);

        for (int i = 0; i < hexCodesArr.length; i++) {
            System.out.println("-----------------" + pc[i] + "---------------------------");

            print(capstone, hexCodesArr[i]);
        }

    }


    private static void print(Capstone capstone, byte[] hexCodes) {
        Capstone.CsInsn[] allInsn = capstone.disasm(hexCodes, addr);

        for (Capstone.CsInsn csInsn : allInsn) {
            System.out.printf("0x%x:\t%s\t\t%s\n", csInsn.address,
                    csInsn.mnemonic, csInsn.opStr);
        }


    }

    // demo/MathGame|primeFactors|(I)Ljava/util/List;|24
    public static String[] splitInvokeInfo(String invokeInfo) {
        int index1 = invokeInfo.indexOf('|');
        int index2 = invokeInfo.indexOf('|', index1 + 1);
        int index3 = invokeInfo.indexOf('|', index2 + 1);
        return new String[]{invokeInfo.substring(0, index1), invokeInfo.substring(index1 + 1, index2),
                invokeInfo.substring(index2 + 1, index3), invokeInfo.substring(index3 + 1)};
    }

    @Test
    public void testSplitInvokeInfo() throws Throwable {
        String[] strings = splitInvokeInfo("demo/MathGame|primeFactors|(I)Ljava/util/List;|24");

    }
}

