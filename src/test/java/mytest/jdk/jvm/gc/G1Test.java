package mytest.jdk.jvm.gc;

import java.io.IOException;
import java.util.Random;

/**
 * @Author dwang
 *  -verbose:gc -Xms20m -Xmx20m -XX:+UseG1GC -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:MaxGCPauseMillis=200m
 * @Description 测试g1 young gc
 * @create 2022/2/15 14:39
 * @Modified By:
 */
public class G1Test {

    public static void main(String[] args) {
        int size = 1024*1 ;
//        String[] myAlloc1 = new String[size];
//        String[] myAlloc2 = new String[size];
//        String[] myAlloc3 = new String[size];
//        String[] myAlloc4 = new String[size];
//        String[] myAlloc5 = new String[size];
        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        String[] myAlloc6 = new String[size];
                        for (int i = 0; i < myAlloc6.length; i++) {
                            myAlloc6[i] = i+"";
                        }
//                        myAlloc6 = null;
//                    System.out.println("gc? -> " + Thread.currentThread().getName());
                        try {
                            Thread.sleep(new Random().nextInt(10000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        myAlloc6 = new String[size];
                    }
                }
            }).start();
        }

        //
//        try {
//            System.in.read();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

}
