package mytest.jdk.jvm.gc;

import java.io.IOException;

/**
 * -Xms10m -Xmx10m -XX:+PrintGCDetails -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps
 * @Author dwang
 * @Description 测试g1 young gc
 * @create 2022/2/15 14:39
 * @Modified By:
 */
public class CMSTest {

    public static void main(String[] args) {
//        test1();
        test2();
    }

    // 可以出现promotion failed 和
    private static void test2() {
        int size = 1024 * 1024;
        byte[] myAlloc1 = new byte[size];
        byte[] myAlloc2 = new byte[size];
        byte[] myAlloc3 = new byte[size];
        byte[] myAlloc4 = new byte[size];
        byte[] myAlloc5 = new byte[size];
        byte[] myAlloc6 = new byte[size];

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试会出现CMS收集
     */
    private static void test1() {
        int size = 1024 * 512;
        byte[] myAlloc1 = new byte[size];
        byte[] myAlloc2 = new byte[size];
        byte[] myAlloc3 = new byte[size];
        byte[] myAlloc4 = new byte[size];
        byte[] myAlloc5 = new byte[size];
        byte[] myAlloc6 = new byte[size];
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
