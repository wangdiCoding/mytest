package mytest.jdk.jvm.escapeAnalysis;

/**
 * @Author dwang
 * @Description 原文链接：https://blog.csdn.net/yangzl2008/article/details/43202969
 * @create 2022/2/14 17:03
 * @Modified By:
 */
public class EscapeAnalysis {
    private static class Foo {
        private int x;
        private static int counter;

        public Foo() {
            x = (++counter);
        }
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        for (int i = 0; i < 1000 * 1000 * 10; ++i) {
            Foo foo = new Foo();
        }
        long end = System.nanoTime();
        System.out.println("Time cost is " + (end - start));
    }

}
