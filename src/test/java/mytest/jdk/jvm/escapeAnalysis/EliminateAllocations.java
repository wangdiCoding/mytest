package mytest.jdk.jvm.escapeAnalysis;

/**
 * @Author dwang
 * @Description 标量替换的测试：把类中的参数如果没有逃逸出去会变成局部变量
 *
 *
 *
 * 下面的调用会变成这样： 但是看到具体的细节因为不能被打印
 * private static void alloc() {
 *    int x = 1;
 *    int y = 2;
 *    System.out.println("point.x="+x+"; point.y="+y);
 * }
 *
 * @create 2022/9/1 15:35
 * @Modified By:
 */
public class EliminateAllocations {
    public static void main(String[] args) {
        new EliminateAllocations().alloc();
    }

    private  void alloc() {
        Point point = new Point(1,2);
        System.out.println("point.x="+point.x+"; point.y="+point.y);
    }
    class Point{
        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
