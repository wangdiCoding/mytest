package mytest.jdk.io;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author dwang
 * @Description 随机IOTest,更灵活
 * RandomAccessFile
 * FileChannel
 * MappedByteBuffer
 * https://blog.csdn.net/xiaoduanayu/article/details/106990972
 *
 * @create 2022/3/10 14:53
 * @Modified By:
 */
public class RandomIoTest {

    @Test
    public void RandomAccessFileTest() throws Exception {

        RandomAccessFile raf = new RandomAccessFile("E:\\tmp.txt", "rw");
        String msg = "你好，world!";
        System.out.println("初始文件指针位置：" + raf.getFilePointer());
        // 指针指向的是字节数的位置
        raf.write(msg.getBytes());
        System.out.println("第一次写完之后文件指针位置：" + raf.getFilePointer());
        // 手动修改读写指针位置
        raf.seek(9);
        System.out.println("修改后的文件指针位置：" + raf.getFilePointer());
        byte b = raf.readByte();
        System.out.println("第一次读完之后文件指针位置：" + raf.getFilePointer() + "，读到的内容：" + (char) b);
        // 修改指针位置10处的字节内容
        raf.writeBytes("K");
        System.out.println("第二次写完之后文件指针位置：" + raf.getFilePointer());
        byte[] dst = new byte[(int) raf.length()];
        // 指针归零
        raf.seek(0);
        // 将数据全部读取出来
        raf.read(dst);
        System.out.println("第二次读完之后文件指针位置：" + raf.getFilePointer() + "，读到的内容：" + new String(dst));
        raf.close();

    }

    @Test
    public void file_channel() throws Exception {
        RandomAccessFile raf = new RandomAccessFile("E:\\tmp.txt", "rw");
        FileChannel fileChannel = raf.getChannel();
        String msg = "你好，world!";
        System.out.println("初始文件指针位置：" + fileChannel.position());
        fileChannel.write(ByteBuffer.wrap(msg.getBytes()));
        System.out.println("第一次写完之后文件指针位置：" + fileChannel.position());
        // 手动修改读写指针位置
        fileChannel.position(9);
        System.out.println("修改后的文件指针位置：" + fileChannel.position());
        ByteBuffer dst = ByteBuffer.allocate(1);
        fileChannel.read(dst);
        dst.flip();
        System.out.println("第一次读完之后文件指针位置：" + fileChannel.position() + "，读到的内容：" + (char) dst.get());
        // 修改指针位置10处的字节内容
        fileChannel.write(ByteBuffer.wrap("K".getBytes()));
        System.out.println("第二次写完之后文件指针位置：" + fileChannel.position());
        dst = ByteBuffer.allocate((int) fileChannel.size());
        // 指针归零
        fileChannel.position(0);
        // 将数据全部读取出来
        fileChannel.read(dst);
        dst.flip();
        System.out.println("第二次读完之后文件指针位置：" + fileChannel.position() + "，读到的内容：" + new String(dst.array()));
        fileChannel.close();
    }

    /**
     * mmap系统调用可以将文件的一个指定区域直接映射到用户进程的虚拟地址空间，这样当用户进程操作文件时，就像操作分配给自己的内存一样。
     * 更详细地说，就是以普通方式去读写文件时，会产生 read/write 系统调用，而通过 mmap 方式操作文件时，在文件读写的过程中不会产生 read/write 系统调用。
     * @throws Exception
     */
    @Test
    public void mmap_test() throws Exception {
        RandomAccessFile raf = new RandomAccessFile("E:\\tmp.txt", "rw");
        FileChannel fileChannel = raf.getChannel();

        String msg = "happy";
        byte[] bytes = msg.getBytes();
        long size = bytes.length * 10000000L;
        long start = System.currentTimeMillis();


        MappedByteBuffer map = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, size);
        for (int i = 0; i < 10000000L; i++) {
            map.put(bytes);
        }
        raf.close();
        long end = System.currentTimeMillis();
        System.out.println("耗时，time = " + (end - start));
    }



}
