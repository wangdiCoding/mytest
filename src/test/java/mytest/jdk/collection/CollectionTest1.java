package mytest.jdk.collection;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description
 * @ClassName CollectionTest1
 * @Author wangDi
 * @date 2020-12-22 15:56
 */
public class CollectionTest1 {

    /**
     * 比较for循环和iterator的使用
     */
    @Test
    void test1() {
//        List<String> strings = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            integerList.add(i);
        }

        for (int i = 0; i < integerList.size(); i++) {
            System.out.print(integerList.get(i));
        }
        integerList.forEach(s -> System.out.print(s));

        System.out.println();

        // 迭代器解决边删除边遍历场景
        Iterator<Integer> iterator = integerList.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (next % 10 == 0) {
                iterator.remove();
            }
        }
        integerList.forEach(s -> System.out.print(s));

/*
        // for从后向前遍历删除场景,因为size是初始赋值不会变，
        for (int i = integerList.size()-1; i >=0 ; i--) {
            Integer integer = integerList.get(i);
            if (integer % 10 == 0) {
                integerList.remove(i);
            }
        }
        integerList.forEach(s -> System.out.print(s));

        //从前往后就会一直去比较integerList的size
        for (int i = 0; i < integerList.size(); i++) {

        }*/


    }

    @Test
    void test_id_is_null() {
        ArrayList<CollectData> collectData = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            collectData.add(new CollectData(i+"", "name="+i));
        }


        List<String> collect = collectData.stream().filter(data-> StringUtils.isNotBlank(data.getId())).map(data -> data.getId()).collect(Collectors.toList());
        System.out.println(collect);

    }

    @Test
    void test_steam() {
        ArrayList<CollectData> collectData = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            collectData.add(new CollectData(i+"", "name="+i));
        }

        //多出来两个null 问题应该不大
        List<CollectData> collect = collectData.stream().collect(Collectors.toList());
        Set<CollectData> dataSet = collectData.stream().collect(Collectors.toSet());


        for (int i = 0; i < collect.size(); i++) {

            System.out.println(collect.get(i));
        }


        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

        objectObjectHashMap.put(1,1);
    }


}
