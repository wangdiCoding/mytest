package mytest.jdk.collection.map;


import java.security.PrivateKey;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @Author dwang
 * @Description 自定义Key。为了相同的HashCode,触发拉链法
 * @create 2022/1/12 14:33
 * @Modified By:
 */
public class MapKey {
    private static final String REG ="[0-9]+";

    private String key;


    public MapKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapKey mapKey = (MapKey) o;
        return Objects.equals(key, mapKey.key);
    }

    /**
     * 模拟 出现hash冲突使用拉链法
     * @return
     */
    @Override
    public int hashCode() {
        if (key == null) {
            return 0;
        }
        Pattern compile = Pattern.compile(REG);
        if (compile.matcher(key).matches()) {
            return 1;
        } else {
            return 2;
        }
    }
}
