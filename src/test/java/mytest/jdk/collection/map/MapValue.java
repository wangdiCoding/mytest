package mytest.jdk.collection.map;

/**
 * @version 1.0
 * @ClassName MapValue
 * @Description 测试是否需要可比
 * @Author wangdi
 * @Date 2022/1/17 20:56
 **/

public class MapValue {
    private String value;

    public MapValue(String value) {
        this.value = value;
    }
}
