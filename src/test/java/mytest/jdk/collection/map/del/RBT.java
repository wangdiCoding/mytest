package mytest.jdk.collection.map.del;

import java.util.LinkedList;
import java.util.Queue;

public class RBT<E extends Comparable<E>> {

    private static final boolean RED = true;
    private static final boolean BLACK = false;

    public Node root;
    private int size;

    public int size() {
        return size;
    }


    /**
     * 功能描述 删除结点
     * <p>
     * 理解参考：
     * https://blog.csdn.net/qq_40843865/article/details/102498310
     * <p>
     * 结点有三种情况：叶子结点，有一个孩子，有两个孩子
     * 颜色有两种：红黑
     * 排列组合一共有3X2 = 6种情况
     * 六种情况有一种是无效的：被删除节点如果只有一个节点，删除节点必为黑节点，子节点必为红（否则不满足规则5）
     *
     *
     * 实现参考
     * https://www.cnblogs.com/GNLin0820/p/9668378.html
     *
     * 我的理解是：
     * 1. 删除和调整是两个过程，有一些情况只移动不删除，比如黑色叶子节点的全员黑色
     * 2. 旋转变色的顺序可以交换，所以统一变色，再旋转比较好理解
     * 3. 调整树平衡目的就是黑节点的高度一样，也就是要满足红黑树的规则5 （这一点也是最重要的一点！！！）
     *
     *
     *
     *
     * 4.注意叶子结点是黑色结点的删除（因为会破坏黑节点平衡，所以复杂）
     *   1.优先从侄子节点借红色（3、4）
     *   2.侄子节点没有红色，则从兄弟节点借红色（2）
     *   3.兄弟节点也没有红色，则从父节点借红色（1）
     *   4.连父节点也没有红色，则从祖辈节点借红色（5）
     *   从祖辈节点借红色时，将父节点当做当前节点递归判断
     *
     * 5. 注意有旋转需要修改parent的指向
     *
     * @return
     * @author wangdi
     * @date 2022/1/17 22:35
     */
    public boolean delete(int elem) {
        if (null == this.root) {
            return Boolean.FALSE;
        } else {
            Node node = this.root;

            while (null != node) {
                Integer e = (Integer) node.e;
                if (e == elem) {
                    // 找到节点
                    deleteNode(node);
                    return true;
                } else if (e > elem) {
                    node = node.left;
                } else {
                    node = node.right;
                }
            }
            return false;
        }
    }

    void deleteNode(Node node) {

        // 删除叶子结点
        if (node.left == null && node.right == null) {

            // 叶子结点是红的,直接删掉即可 不影响高度
            if (isRed(node)) {
                deleteRedLeft(node, true);
            } else {
                // 最难后面单独讨论
                deleteBlack(node, true);
            }

            // 只有一个孩子，那么这个被删的结点一定是黑的，孩子结点一定是红的，直接把孩子接上来完事
            // 有右孩子
        } else if (null == node.left) {
            node.e = node.right.e;
            node.right = null;

            // 有左孩子，同理直接拉孩子上来
        } else if (null == node.right) {
            node.e = node.left.e;
            node.left = null;


            // 有两个孩子的节点  和bst一样： 替换结点 -> 找到后置结点(右节点最左边的孩子) -》交换结点 -》删除被交换的节点（最多只有有孩子，或者是叶子节点）
        } else {
            Node next = node.right;
            while (null != next.left) {
                next = next.left;
            }
            // 交换两个结点的所有信息
            swapNode(node, next);

            // 删除next
            deleteNode(next);

        }
    }

    /**
     * 黑色叶子节点删除 --- 一共五种情况
     * 需要考虑的节点关系：找出兄弟节点，找出远侄子节点，找出近侄子节点， 以及父亲节点
     *
     * 情况1 父节点P是红色节点， 节点删除，父节点变黑，兄弟变红，保证平衡
     *         p(red)
     *      /          \
     *    D(black)     S(black)
     * 1.节点删除
     * 2. 父节点变黑
     * 3. 兄弟节点变红
     * 对应deleteAction1()
     *
     *-----
     * 情况2  兄弟结点是红的，兄弟一定有黑孩子（保证高度）  把兄弟变黑，父亲变红，然后把兄弟转到父节点，转过来之后变成情况1(父节点红色)
     *
     * 1. 父节点变红
     * 2. 兄弟节点边黑
     * 3.删除节点在左边就兄弟节点左旋 ，右边就右旋
     * 对应deleteAction2()
     *
     *
     * ---
     * 情况3   兄弟黑，但是兄弟的远亲侄子红 ，父亲随意颜色
     *                p(随意)
     *              /        \
     *           D(b)         S(b)
     *                         \
     *                         SR(R)
     *   交换P和S的颜色，然后把远侄子节点SR/SL设置为黑色,然后旋转过来变成,自平衡的不用管他
     *         s(随意)
     *      /          \
     *   p(black)        SR(black)
     *
     * ---
     * 情况4  兄弟黑，但是兄弟的近亲侄子红 ，父亲随意颜色
     *                p(随意)
     *              /        \
     *           D(b)         S(b)
     *                        /
     *                      SL(R)
     *
     *  S染红，然后把近侄子节点SR/SL设置为黑色 ,然后右旋S变成情况3
     *                p(随意)
     *              /        \
     *           D(b)         SL(b)
     *                         \
     *
     *    S(R)
     * ---
     *  情况5  父亲黑，兄弟也黑 没侄子，因为有侄子要么是情况3.4，不然高度不对
     *  d删掉，兄弟变红，(目的 保证黑高度不变)
     *  然后递归往上，把p当做d进行递归，但是不删掉(needDel控制)。只是把对应的兄弟变红而已，
     *  就可能会出现之前1-4种被删除的节点是黑的的情形（这时候没有被删除的节点，只是把P当成D 来进行操作而已）
     *
     *  举个例子，
     *  1.如果节点全是黑色的，则一直递归到根节点黑色停止
     *  2.如果p的p 也就是GP是红色的，则会对应第一种情况(父节点是红，)
     *
     *            /
     *         GP(red)
     *      /          \
     *    P(black)     PS(black)      ->   如下情况一的不删除P节点，只变色父节点和兄弟节点（保持平衡）
     *         \           \
     *          S(R)        PSR(b)
     *
     *
     *            /
     *         GP(B)
     *      /          \
     *    P(black)     PS(red)
     *         \           \
     *          S(R)        PSR(b)
     *
     *
     *   实现过程中发现的错误：
     *   1. 删除节点操作是统一控制，在deleteAction里面只进行旋转和变色操作。目的就是为了能够单独控制删除
     *   只有实现一遍才会发现里面的是真的有趣
     *
     */
    private void deleteBlack(Node node, boolean needDel) {
        // 先把父亲，兄弟，侄子全找出来
        Node parent = node.parent;
        if (parent != null) {
            boolean nodeInLeft = parent.left == node;
            // 兄弟结点
            Node sibling = nodeInLeft ? parent.right : parent.left;
            // 远侄子
            Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
            // 近侄子
            Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);

            // 开始判断
            // 情况二 ： 兄弟是红的  变色旋转 转成情况一
            if (isRed(sibling)) {
                deleteAction2(node);
                // 情况三 远侄子存在 注意考虑如果近侄子也存在，近侄子需要改一下位置
            } else if (null != remoteNephew && isRed(remoteNephew)) {
                deleteAction3(node);

                // 情况四 近侄子存在，远侄子不存在
            } else if (null != nearNephew && isRed(nearNephew)) {
                deleteAction4(node);

            } else {
                // 情况1 红结点
                if (isRed(parent)) {
                    deleteAction1(node);
                } else {
                    // 情况五 三人黑递归
                    deleteAction5(node);

                }
            }
        }

        // 只有第一次进来才会删除黑叶子节点
        if (needDel) {
            // 只有根
            if (null == parent) {
                this.root = null;
            } else if ( parent.left == node) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        }

    }

    /**
     * 最有趣的一种情况（全员恶人） ,S 肯定也是叶子节点，保证高度平衡
     *
     *                p(b)
     *              /     \
     *           D(b)      S(b)
     *
     *
     *  1.砍掉D  S变红，
     *  2.把p当作d，不删除p, 只让p的兄弟变红，再往上，会变成前面的情况
     *
     *
     * @param node
     */
    private void deleteAction5(Node node) {
        // 父亲
        Node parent = node.parent;
        boolean nodeInLeft = parent.left == node;
        // 兄弟结点
        Node sibling = nodeInLeft ? parent.right : parent.left;
        // 远侄子
        Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
        // 近侄子
        Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);

        // 兄弟变红
        sibling.color = RED;

        // 调整父子树
        deleteBlack(parent, false);
    }

    /**
     * 情况四：兄弟黑，父节点随意，只有近侄子,注意近侄子一定是叶子节点，不然不平衡
     *
     *                p(随意)
     *              /        \
     *           D(b)         S(b)
     *                      /
     *                    SL(R)叶子
     *
     *  想办法转成情况三：S 和近侄子互换颜色，如果D在左边就右旋转成情况三，反之亦然
     *
     *
     *                p(随意)
     *              /        \
     *           D(b)         SL(b)
     *                           \
     *                          S(R)
     *
     * @param node
     */
    private void deleteAction4(Node node) {
        // 父亲
        Node parent = node.parent;
        boolean nodeInLeft = parent.left == node;
        // 兄弟结点
        Node sibling = nodeInLeft ? parent.right : parent.left;
        // 远侄子
        Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
        // 近侄子
        Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);

        // 先S和nearNephew互换颜色
        boolean sColor = sibling.color;
        sibling.color = nearNephew.color;
        nearNephew.color = sColor;

        // 然后兄弟节点旋转
        if (nodeInLeft) {
            // 右旋
            sibling.left = null;
            nearNephew.right = sibling;
        } else {
            // 左旋
            sibling.right = null;
            nearNephew.left = sibling;

        }
        // 修改parent
        nearNephew.parent = sibling.parent;
        sibling.parent = nearNephew;

        // 转成情况3
        deleteAction3(node);
    }

    /**
     * 情况3   兄弟黑，但是兄弟的远亲侄子红 ，父亲随意颜色
     *                p(随意)
     *              /        \
     *           D(b)         S(b)
     *                      /  \
     *                    SL(R)    SR(R)
     *   交换P和S的颜色，然后把远侄子节点SR/SL设置为黑色,然后旋转过来变成自平衡的不需要再操作
     *
     *   1.p的color给s (red/black)
     *   2.s的color给p-> （black）
     *   3.被删除在左边就左旋（注意兄弟近侄子的情况）
     *
     *
     */
    private void deleteAction3(Node node) {
        // 父亲
        Node parent = node.parent;

        boolean nodeInLeft = parent.left == node;
        // 兄弟结点
        Node sibling = nodeInLeft ? parent.right : parent.left;
        // 远侄子
        Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
        // 近侄子
        Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);

        // 交换颜色
        boolean pColor = parent.color;
        parent.color = sibling.color;
        sibling.color = pColor;
        // 远侄子要变色
        remoteNephew.color = BLACK;

        // 旋转，删除在哪边，就哪边旋转
        if (nodeInLeft) {
            //删节点   (错误操作)
            //parent.left = null;

            // 左旋
            parent.right = sibling.left;
            sibling.left = parent;

        // 右旋
        } else {
            //删节点   (错误操作)
            // parent.right = null;


            parent.left = sibling.right;
            sibling.right = parent;
        }
        // 修改parent信息
        // parent.parent 给 s 做为parent
        // s给parent做parent
        sibling.parent = parent.parent;
        parent.parent = sibling;
    }

    /**
     * 功能描述 情况1 父节点红 P染成黑色，此时P左子树的黑色结点数目恢复，但此时右子树黑色结点数目多了一，再把S对应染成红色即可。
     * @author wangdi
     * @date   2022/1/18 0:57
     * @param node
     * @return void
     */
    private void deleteAction1(Node node) {
        // 父亲
        Node parent = node.parent;
        boolean nodeInLeft = parent.left == node;
        // 兄弟结点
        Node sibling = nodeInLeft ? parent.right : parent.left;
        // 远侄子
        Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
        // 近侄子
        Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);

//        // 删除节点
//        if (nodeInLeft) {
//            parent.left = null;
//        } else {
//            parent.right = null;
//        }
        parent.color = BLACK;
        sibling.color = RED;

    }

    /**
     * 情况2  兄弟结点是红的，兄弟一定有黑孩子（保证高度）  把兄弟变黑，父亲变红，然后把兄弟转到父节点，转过来之后变成情况1
     * @author wangdi
     * @date   2022/1/18 0:47
     * @param node
     * @return void
     */
    private void deleteAction2(Node node) {
        // 父亲
        Node parent = node.parent;
        boolean nodeInLeft = parent.left == node;
        // 兄弟结点
        Node sibling = nodeInLeft ? parent.right : parent.left;
        // 远侄子
        Node remoteNephew = null == sibling ? null : (nodeInLeft ? sibling.right : sibling.left);
        // 近侄子
        Node nearNephew = null == sibling ? null : (nodeInLeft ? sibling.left : sibling.right);


        sibling.color = BLACK;
        parent.color = RED;
        // 左旋
        if (nodeInLeft) {
            parent.right = sibling.left;
            sibling.left = parent;

            // 右旋
        } else {
            parent.left = sibling.right;
            sibling.right = parent;
        }

        // 修改parent信息
        // parent.parent 给 s做为parent
        // s给parent做parent
        sibling.parent = parent.parent;
        parent.parent = sibling;


        // 转成情况1
        deleteAction1(node);

    }







    /**
     * 功能描述 删除红结点
     *
     * @param node
     * @param needDel
     * @return void
     * @author wangdi
     * @date 2022/1/17 23:33
     */
    private void deleteRedLeft(Node node, boolean needDel) {
        Node parent = node.parent;
        if (node == parent.left) {
            parent.left = null;
        } else {
            parent.right = null;
        }
    }


    private void swapNode(Node node, Node next) {
        Node temp = new Node(node);

        node.e = next.e;
        node.left = next.left;
        node.right = next.right;
        node.color = next.color;
        node.parent = next.parent;

        next.e = temp.e;
        next.left = temp.left;
        next.right = temp.right;
        next.color = temp.color;
        next.parent = temp.parent;
    }


    //添加节点,递归
    public void addEle(E e) {
        root = addEle(root, e);
        root.color = BLACK;
    }

    /**
     * 将元素E添加到以node为根的那个树上并且将新树的根返回
     *
     * @param node
     * @param e
     * @return
     */
    private Node addEle(Node node, E e) {
        if (node == null) {
            size++;
            return new Node(e);
        }

        if (e.compareTo(node.e) < 0) {
            node.left = addEle(node.left, e);
        } else {
            node.right = addEle(node.right, e);
        }

        //左旋
        if (isRed(node.right) && !isRed(node.left)) {
            node = leftRotate(node);
        }


        //右旋
        if (isRed(node.left) && isRed(node.left.left)) {
            node = rightRotate(node);
        }


        //颜色翻转
        if (isRed(node.right) && isRed(node.left)) {
            flipColors(node);
        }


        return node;
    }

    //   node                     x
    //  /   \     左旋转         /  \
    // T1   x   --------->   node   T3
    //     / \              /   \
    //    T2 T3            T1   T2
    private Node leftRotate(Node node) {

        Node x = node.right;

        node.right = x.left;
        x.left = node;

        x.color = node.color;
        node.color = RED;

        return x;


    }


    //颜色翻转
    private void flipColors(Node node) {
        node.color = RED;
        node.left.color = BLACK;
        node.right.color = BLACK;


    }

    //     node                   x
    //    /   \     右旋转       /  \
    //   x    T2   ------->   y   node
    //  / \                       /  \
    // y  T1                     T1  T2

    private Node rightRotate(Node node) {
        Node x = node.left;

        node.left = x.right;
        x.right = node;

        x.color = node.color;
        node.color = RED;
        return x;

    }


    private boolean isRed(Node node) {
        if (node == null) return BLACK;
        return node.color;
    }


    private class Node {
        public E e;
        public Node left;
        public Node right;
        public Node parent;
        public boolean color;

        public Node(E e) {
            this.e = e;
            this.left = null;
            this.right = null;
            this.color = RED;
        }

        public Node(Node node) {
            this.e = node.e;
            this.left = node.left;
            this.right = node.right;
            this.color = node.color;
            this.parent = node.parent;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "e=" + e +
                    ", color=" + color +
                    '}';
        }
    }


    /**
     * 层次遍历树
     */
    public void levelOrder() {
        if (root == null) {
            return;
        }
        Queue<Node> queue = new LinkedList();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node curNode = queue.remove();
            System.out.println(curNode);
            if (curNode.left != null) {
                queue.add(curNode.left);
            }
            if (curNode.right != null) {
                queue.add(curNode.right);
            }
        }
    }


}
