package mytest.jdk.collection.map.del;

/**
 * HashSet---HashMap ----数组+链表+红黑树     存的是HashMap的key
 * TreeSet---TreeeMap----RBT(NavigableMap)
 */
public class RBTMain {

    public static void main(String[] args) {
        RBT rbt = new RBT();
        rbt.addEle(35);
        rbt.addEle(42);
        rbt.addEle(22);
        rbt.levelOrder();
    }
}
