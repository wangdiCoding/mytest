package mytest.jdk.collection.map;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

/**
 * @Description 学习HashMap
 * @ClassName MyHashTest
 * @Author wangDi
 * @date 2021-05-10 12:01
 */
public class MyHashTest {


    /**
     * 测试put的过程（扩容和树化）
     * 第一步：这个时候桶中bin的数量小于TREEIFY_THRESHOLD（第一次是12）
     * 重写key的hashcode，目的出现hash冲突
     * 目的：put如何解决hash冲突
     *
     * 第二步：这个时候桶中bin的数量大于了TREEIFY_THRESHOLD，但是capacity不大于MIN_TREEIFY_CAPACITY，则要扩容,再treefyBin中扩容。
     * 目的：resize()方法
     *
     * 第三步：capacity达到MIN_TREEIFY_CAPACITY，开始树化
     * 目的：查看树化的过程
     *
     * debug看结构 记得取消勾选setting -> java -> dataview 两个关于collection的选项
     */
    @Test
    public void putTest() {

        HashMap<MapKey, String> hashMap = new HashMap<>();
        // 第一阶段测试
        for (int i = 0; i < 6; i++) {
            hashMap.put(new MapKey(String.valueOf(i)), String.valueOf(i));
        }
        System.out.println(hashMap);

        // 第二阶段测试 ，添加会发生扩容(不需要12次（threshold）,单链大于8也会在treeifyBin发生扩容)
        HashMap<MapKey, String> hashMap2 = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            hashMap2.put(new MapKey(String.valueOf(i)), String.valueOf(i));
        }
        System.out.println(hashMap2);

        // 再次扩容因为50 > 48
        HashMap<MapKey, String> hashMap3 = new HashMap<>();
        for (int i = 0; i < 50; i++) {
            hashMap3.put(new MapKey(String.valueOf(i)), String.valueOf(i));
        }
        System.out.println(hashMap3);

    }


    /**
     * 功能描述  查看如果value不可比较看出现什么问题
     * @author wangdi
     * @date 2022/1/17 20:57
     * @param
     * @return void
     */
    @Test
    public void putTest2() {

        HashMap<MapKey, MapValue> hashMap = new HashMap<>();
        // 第一阶段测试
        for (int i = 0; i < 6; i++) {
            hashMap.put(new MapKey(String.valueOf(i)), new MapValue(String.valueOf(i)));
        }
        System.out.println(hashMap);

        // 第二阶段测试 ，添加会发生扩容(不需要12次（threshold）)
        HashMap<MapKey, MapValue> hashMap2 = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            hashMap2.put(new MapKey(String.valueOf(i)), new MapValue(String.valueOf(i)));
        }
        System.out.println(hashMap2);

        // 再次扩容因为50 > 48
        HashMap<MapKey, MapValue> hashMap3 = new HashMap<>();
        for (int i = 0; i < 50; i++) {
            hashMap3.put(new MapKey(String.valueOf(i)), new MapValue(String.valueOf(i)));
        }
        System.out.println(hashMap3);

    }


    /**
     * debug树化过程
     */
    @Test
    public void test_treeify() {

        HashMap<MapKey, String> hashMap3 = new HashMap<>();
        for (int i = 0; i < 50; i++) {
            hashMap3.put(new MapKey(String.valueOf(i)), String.valueOf(i));
        }
        System.out.println(hashMap3);

    }

    @Test
    public void test_delete_TO_link() {
        HashMap<MapKey, MapValue> treeify = new HashMap<>();
        for (int i = 0; i < 11; i++) {
            treeify.put(new MapKey(String.valueOf(i)), new MapValue(String.valueOf(i)));
        }

        System.out.println(treeify);

        int threadhold = 8;
        for (int i = 0; i < threadhold; i++) {
            if (i < threadhold - 1) {

                treeify.remove(new MapKey(String.valueOf(i)));
            } else {
                // 这个会触发退树
                treeify.remove(new MapKey(String.valueOf(i)));

            }
        }
        System.out.println(treeify);

    }


}
