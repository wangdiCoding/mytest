package mytest.jdk.collection.lists.multiThread;

import org.junit.jupiter.api.Test;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author dwang
 * @Description 测试List 在并发下的问题
 * @create 2022/1/12 13:34
 * @Modified By:
 */
public class MultiThreadTest {

    private static List<Integer> list = new ArrayList<>();

    private static ExecutorService executorService = Executors.newFixedThreadPool(1000);
    /**
     * add 方法的并发问题:
     *
     *     public boolean add(E e) {
     *         ensureCapacityInternal(size + 1);  // Increments modCount!!
     *         elementData[size++] = e;
     *         return true;
     *     }
     *
     *  在多线程，这个size会有问题
     *  elementData[size++] = e;
     *
     * 模拟1000个线程,，每个线程加100次。最后list 长度应该为1000*100
     */
    @Test
    public void testAdd(){
        System.out.println("list size is:" + list.size());
        for (int i = 0; i < 1000; i++) {
            // 执行任务
            executorService.submit(() -> {
//                System.out.println("ThreadId:"+Thread.currentThread().getId() +"start!");
                for (int j = 0; j < 1000 ; j++) {
                    list.add(j);
                }
//                System.out.println("ThreadId:"+Thread.currentThread().getId() +"end!");
            });
        }
        executorService.shutdown();

        while (!executorService.isTerminated()) {
            try {
                Thread.sleep(1000*5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("all task finish");
        System.out.println("预计存储大小为： " + 1000*100);
        System.out.println("list size is:" + list.size());

    }


    /**
     * 测试在多线程的条件下：
     * 扩容可能产生数据下标越界的异常
     *
     * 每个线程循环1000000次添加元素
     *
     */
    @Test
    public void test2() throws InterruptedException {
        new Thread(() -> {
            System.out.println("ThreadId: "+Thread.currentThread().getId() +" start!");
            for (int j = 0; j < 10000000 ; j++) {
                list.add(j);
            }
            System.out.println("ThreadId:"+Thread.currentThread().getId() +" end!");
        }).start();
        new Thread(() -> {
            System.out.println("ThreadId: "+Thread.currentThread().getId() +" start!");
            for (int j = 0; j < 10000000 ; j++) {
                list.add(j);
            }
            System.out.println("ThreadId: "+Thread.currentThread().getId() +" end!");
        }).start();
        System.out.println(list.size());
        Thread.sleep(1000*10);
    }

}
