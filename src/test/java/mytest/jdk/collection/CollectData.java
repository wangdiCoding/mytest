package mytest.jdk.collection;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/9/20 16:48
 * @Modified By:
 */
public class CollectData {
    String id;
    String name;

    public CollectData(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
