package mytest.jdk.thread.status;

/**
 * @Author dwang
 * @Description wait和notify的线程的状态
 * @create 2023/5/23 16:23
 * @Modified By:
 */
class MyThread extends Thread {

    Thread main;

    public MyThread(Thread main) {
        this.main = main;
    }

    public void run() {
        synchronized (this) {
            WaitAndNotifyDemo.showStatus(main);
            System.out.println("before notify");
            notify();
            System.out.println("after notify");
            WaitAndNotifyDemo.showStatus(main);

        }
    }
}

public class WaitAndNotifyDemo {
    public static void main(String[] args) throws InterruptedException {

        MyThread myThread = new MyThread(Thread.currentThread());

        synchronized (myThread) {
            try {
                myThread.start();
                Thread.sleep(1000);
                showStatus(myThread);
                // 主线程睡眠3s
                Thread.sleep(3000);
                System.out.println("before wait");
                // 阻塞主线程
                myThread.wait();
                System.out.println("after wait");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static  void showStatus(Thread t){
        System.out.println("线程名称："+t.getName() + "-----线程状态："+ t.getState());
    }
}

