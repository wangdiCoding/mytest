package mytest.jdk.thread.status;


/**
 * 线程创建方式
 * 继承Thread
 * 实现Runnable
 * 实现Callable接口
 * 线程池
 *
 * 线程状态
 * NEW(初始):尚未启动的线程的线程状态。
 * RUNNABLE(运行):可运行线程的线程状态。
 * BLOCKED(阻塞):线程的线程状态被阻止，正在等待监视器锁。
 * WAITING(等待):等待线程的线程状态。
 * TIMED_WAITING(超时等待):具有指定等待时间的等待线程的线程状态。
 * TERMINATED(终止):终止线程的线程状态。
 *
 */
public class ThreadStatus {

    private static Object resource1 = "资源1";
    private static Object resource2 = "资源2";


    public static void main(String[] args) throws InterruptedException {



        //模拟死锁，死锁两个线程就是在阻塞状态
        // thread.sleep的状态是time_waiting
        test_t1_t2();

        //
//        test_t3_t4();

    }

    private static void test_t3_t4() throws InterruptedException {
        final Object lock = new Object();
        Thread t3 = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        };
        Thread t4 = new Thread() {
            public void run() {
                synchronized (lock) {
                    lock.notifyAll();
                }
            }
        };
        t3.setName("线程3");
        t4.setName("线程4");
        t3.start();
        t4.start();
        showStatus(t3);
        showStatus(t4);
        Thread.sleep(5000);
        showStatus(t3);
        synchronized (lock){
            lock.notifyAll();
            showStatus(t3);
        }
        Thread.sleep(1000);
        showStatus(t3);
    }


    public static void test_t1_t2() throws InterruptedException {
        ThreadStatusDemo t1 = new ThreadStatusDemo(resource1, resource2);
        t1.setName("线程1");
        showStatus(t1);

        t1.start();
        showStatus(t1);

        ThreadStatusDemo t2 = new ThreadStatusDemo(resource2, resource1);
        t2.setName("线程2");
        t2.start();
        Thread.sleep(1000);
        showStatus(t1);
        showStatus(t2);

        Thread.sleep(3000);
        showStatus(t1);
        showStatus(t2);

        System.out.println("------------------------------");
    }





    public static  void showStatus(Thread t){
        System.out.println("线程名称："+t.getName() + "-----线程状态："+ t.getState());
    }
}

class ThreadStatusDemo extends Thread {

    private Object resource1;
    private Object resource2;

    public ThreadStatusDemo(Object resource1, Object resource2) {
        this.resource1 = resource1;
        this.resource2 = resource2;
    }

    public void run() {
        synchronized (resource1) {
            try {
                System.out.println(Thread.currentThread().getName()+ "will sleep");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (resource2) {

            }
        }
    }
}