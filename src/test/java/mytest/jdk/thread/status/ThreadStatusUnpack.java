package mytest.jdk.thread.status;

import com.codahale.metrics.EWMA;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.LockSupport;

/**
 * @Author dwang
 * @Description pack之后的线程状态 WAITING
 * @create 2022/4/7 17:07
 * @Modified By:
 */
public class ThreadStatusUnpack {

    @Test
    public void status_pack() {


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("start pack");
                LockSupport.park(this);
                System.out.println("start unpack");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        Thread.State state = thread.getState();
        System.out.println("pack thread stats is " + state);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        state = thread.getState();
        System.out.println("pack thread stats is " + state);


        LockSupport.unpark(thread);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        state = thread.getState();
        System.out.println("pack thread stats is " + state);



    }

}
