package mytest.jdk.thread;

/**
 * @Description
 * @ClassName InterruptDemo
 * @Author wangDi
 * @date 2021-04-26 15:33
 */
public class InterruptDemo {

    /**
     * 循环可以中断，
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        //sleepThread睡眠1000ms
        final Thread sleepThread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("sleep thread interrupted");
                }
                super.run();
            }
        };

        //busyThread一直执行死循环,中断并不会停下来
        Thread busyThread = new Thread(() -> {
            while (true) {
//                System.out.println("busyThread isInterrupted: " + Thread.currentThread().isInterrupted());
            }
        });
        sleepThread.start();
        busyThread.start();
        sleepThread.interrupt();
        busyThread.interrupt();
        while (sleepThread.isInterrupted()){
            System.out.println("1");
        };

        System.out.println("sleepThread isInterrupted: " + sleepThread.isInterrupted());
        System.out.println("busyThread isInterrupted: " + busyThread.isInterrupted());


    }
}
