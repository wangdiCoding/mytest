package mytest.jdk.function.test1;

/**
 * @version 1.0
 * @ClassName ThrowExceptionFunction
 * @Description 定义一个抛出异常的形式的函数式接口, 这个接口只有参数没有返回值是个消费型接口
 * @Author wangdi
 * @Date 2021/11/25 21:42
 **/

/**
 * 抛异常接口
 **/
@FunctionalInterface
public interface ThrowExceptionFunction {

    /**
     * 抛出异常信息
     *
     * @param message 异常信息
     * @return void
     **/
    void throwMessage(String message);
}
