package mytest.jdk.function.test2;

/**
 * 分支处理接口
 **/
@FunctionalInterface
public interface MyBranchHandle {

    /**
     * 分支操作
     *
     * @param trueHandle 为true时要进行的操作
     * @param falseHandle 为false时要进行的操作
     * @return void
     **/
    void trueOrFalseHandle(Runnable trueHandle, Runnable falseHandle);

}
