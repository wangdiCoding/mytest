package mytest.jdk.function;

import mytest.jdk.function.test1.ThrowExceptionFunction;
import mytest.jdk.function.test2.MyBranchHandle;
import mytest.jdk.function.test3.PresentOrElseHandler;

import java.util.function.Consumer;

/**
 * @version 1.0
 * @ClassName VUtils
 * @Description todo
 * @Author wangdi
 * @Date 2021/11/25 21:43
 **/

public class VUtils {

    /**
     *  如果参数为true抛出异常
     *
     * @param b
     * @return com.example.demo.func.ThrowExceptionFunction
     **/
    public static ThrowExceptionFunction isTure(boolean b){

        return errorMessage -> {
            if (b){
                throw new RuntimeException(errorMessage);
            }
        };
    }

    public static MyBranchHandle ifTool(Boolean b){

        return new MyBranchHandle() {
            @Override
            public void trueOrFalseHandle(Runnable trueHandle, Runnable falseHandle) {
                if (b) {
                    trueHandle.run();
                } else {
                    falseHandle.run();
                }
            }
        };
    }

    /**
     * 功能描述  为空就执行消费者有返回值，不为空执行runable
     * @author wangdi
     * @date   2021/11/25 22:01
     * @param str
     * @return mytest.jdk.function.test3.PresentOrElseHandler<?>
     */
    public static PresentOrElseHandler<?> blankOrNotBlank(String str) {

        return (consumer, runnable) -> {
            if (str == null || str.length() == 0){
                runnable.run();
            } else {
                consumer.accept(str);
            }
        };

    }

}
