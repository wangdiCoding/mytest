package mytest.jdk.function;

import mytest.jdk.function.test1.ThrowExceptionFunction;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

/**
 * @version 1.0
 * @ClassName FunctionTest
 * @Description  函数式编程解决if  else 问题
 * @Author wangdi
 * @Date 2021/11/25 21:44
 **/

public class FunctionTest {


    /**
     * 功能描述  函数式编程初步使用，
     * @author wangdi
     * @date   2021/11/25 21:45
     * @param
     * @return void
     */
    @Test
    void test1() {
        Boolean tag = Boolean.FALSE;
//        ThrowExceptionFunction throwExceptionFunction = VUtils.isTure(Boolean.TRUE);
        ThrowExceptionFunction throwExceptionFunction = VUtils.isTure(tag);
        throwExceptionFunction.throwMessage("出现异常了兄弟");

        // 代替了
        if (tag){
            throw new RuntimeException("出现异常了兄弟");
        }
    }


    /**
     * 功能描述 branchHandler
     * @author wangdi
     * @date   2021/11/25 21:53
     * @param
     * @return void
     */
    @Test
    void test2() {
        boolean tag = Boolean.TRUE;

        VUtils.ifTool(tag).trueOrFalseHandle(new Runnable() {
            @Override
            public void run() {
                System.out.println("判断true方法");
            }
        }, new Runnable() {
            @Override
            public void run() {
                System.out.println("判断false的方法");
            }
        });
    }

    @Test
    void  test3() {
        String str = "123123qweqwe asd asd ";
        VUtils.blankOrNotBlank(str).presentOrElseHandle(
                new Consumer() {
                    @Override
                    public void accept(Object o) {
                        System.out.println(o);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("打印的空串");
                    }
                }

        );


    }




}
