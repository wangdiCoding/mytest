package mytest.jdk.function.test3;

import java.util.function.Consumer;

/**
 * @version 1.0
 * @ClassName PresentOrElseHandler
 * @Description 如果存在值执行消费操作，否则执行基于空的操作
 * @Author wangdi
 * @Date 2021/11/25 21:57
 **/

public interface PresentOrElseHandler<T extends Object> {


    /**
     * 值不为空时执行消费操作
     * 值为空时执行其他的操作
     *
     * @param action 值不为空时，执行的消费操作
     * @param emptyAction 值为空时，执行的操作
     * @return void
     **/
    void presentOrElseHandle(Consumer<? super T> action, Runnable emptyAction);
}
