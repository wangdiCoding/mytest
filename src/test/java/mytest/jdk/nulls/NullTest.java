package mytest.jdk.nulls;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/1/24 13:56
 * @Modified By:
 */
public class NullTest {

    /**
     * 最low的写法
     */
    @Test
    public void null_check1(){
        SomeObj obj = new SomeObj();

        if (obj != null) {
          obj.doSomething();
        }
    }

    /**
     * 模拟null判断
     * 1. null是不合理的情况
     *  assert判断
     */
    @Test
    public void null_invalid(){

//        SomeObj obj = new SomeObj();
        SomeObj obj = null;
        assert obj != null : "铁子拉杂";
        obj.doSomething();

    }
    /**
     * 模拟null判断
     * 1. null是不合理的情况
     *  直接抛出异常好了
     */
    @Test
    public void null_invalid2(){
        SomeObj obj = null;
        obj.doSomething();
    }

    /**
     * 模拟null判断
     * null合理的情况
     *  1.集合返回空list
     */
    @Test
    public void null_valid1(){
        List<SomeObj> obj = Collections.emptyList();

        for (int i = 0; i < obj.size(); i++) {
            System.out.println(obj);
        }
    }
    /**
     * 模拟null判断
     * null合理的情况
     *  1.对象，给默认操作
     */
    @Test
    public void null_valid2(){
        SomeObj obj = findObj("userId");
        obj.doSomething();

        SomeObj obj2 = findObj("ADADWQDADASD");
        obj2.doSomething();
    }

    private SomeObj findObj(String userId) {
        if ("userId".equals(userId)) {
            return new SomeObj();
        }
        // return default obj
        return new DefaultObj();
    }


}
