package mytest.jdk.nulls;

/**
 * @Author dwang
 * @Description 默认的obj
 * @create 2022/1/24 14:10
 * @Modified By:
 */
public class DefaultObj extends SomeObj {
    @Override
    public void doSomething() {
        System.out.println("default action");
    }
}
