package mytest.jdk.date.learn;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author dwang
 * @Description localdatetime没有线程安全问题
 * @create 2022/4/6 13:56
 * @Modified By:
 */
public class ThreadSafe2 {

    public static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(10);
    DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    //    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static void main(String[] args) throws InterruptedException {
        HashMap<String,Integer> map = new HashMap();
        int threadNum = 1000;
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            THREAD_POOL.submit(new Runnable() {
                @Override
                public void run() {
                    String date = new ThreadSafe2().date(finalI);
                    System.out.println(finalI + " " + date);
                    Integer o = map.get(date);
                    if (o != null) {
                        String s = "并发问题！ key: %s, map中的value:%d, 当前value:%d";

                        try {
                            String format = String.format(s, date,o, finalI);
                            System.out.println(format);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        map.put(date, finalI);
                    }
                    countDownLatch.countDown();
                }
            });
        }
        // 关闭线程池，此种关闭方式不再接受新的任务提交，等待现有队列中的任务全部执行完毕之后关闭
        THREAD_POOL.shutdown();

        countDownLatch.await();
    }

    private String date(int seconds) {
        LocalDateTime date = LocalDateTime.now().plusSeconds(seconds);
        // format不是线程安全的
        return DATE_FORMAT.format(date);
    }
}
