package mytest.jdk.date.learn;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author dwang
 * @Description 演示不使用threadLocal可能产生的多线程的问题SimpleDateFormat
 * @create 2022/4/6 13:56
 * @Modified By:
 */
public class ThreadSafe {

    public static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(10);
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws InterruptedException {
        HashMap<String,Integer> map = new HashMap();
        int threadNum = 1000;
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            THREAD_POOL.submit(new Runnable() {
                @Override
                public void run() {

                    String date = new ThreadSafe().date(finalI);
                    System.out.println(finalI + " " + date);
                    Integer o = map.get(date);
                    if (o != null) {
                        String s = "并发问题！ key: %s, map中的value:%d, 当前value:%d";

                        try {
                            String format = String.format(s, date,o, finalI);
                            System.out.println(format);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        map.put(date, finalI);
                    }
                    countDownLatch.countDown();

                }
            });
        }
        // 关闭线程池，此种关闭方式不再接受新的任务提交，等待现有队列中的任务全部执行完毕之后关闭
        THREAD_POOL.shutdown();

        countDownLatch.await();
    }

    private String date(int seconds) {
        // 参数的单位是毫秒，从1970.1.1 00:00:00 GMT计时
        Date date = new Date(1000 * seconds);

        // format不是线程安全的
        return DATE_FORMAT.format(date);
    }
}
