package mytest.jdk.classes.erase;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @Author dwang
 * @Company neldtv
 * @Description  泛型类型取最小的同级类型
 * @create 2021/10/19 15:37
 * @Modified By:
 */
public class EraseTypeTest3 {
    public static void main(String[] args) {
        Integer y1 = EraseTypeTest3.add(1, 2);
        Number y2 = EraseTypeTest3.add(1, 1.2d);
        Comparable<? extends Comparable<?>> y3 = EraseTypeTest3.add(1, LocalDate.now());

        System.out.println(y3.getClass());
    }

    public static <T> T add(T x ,T y) {
        return y;
    }
}
