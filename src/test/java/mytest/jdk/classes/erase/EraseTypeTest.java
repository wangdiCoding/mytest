package mytest.jdk.classes.erase;

import java.util.ArrayList;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 类型擦除问题1 - 比较类是相等的呢
 * @create 2021/10/19 15:37
 * @Modified By:
 */
public class EraseTypeTest {

    public static void main(String[] args) {

        Class c1 = new ArrayList<String>().getClass();
        Class c2 = new ArrayList<Integer>().getClass();
        System.out.println(c1 == c2);
    }
}
