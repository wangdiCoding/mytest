package mytest.jdk.classes.erase;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * @Author dwang
 * @Company neldtv
 * @Description 类型擦除问题2 可以添加不同的数据
 * @create 2021/10/19 15:37
 * @Modified By:
 */
public class EraseTypeTest2 {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.getClass().getMethod("add", Object.class).invoke(integers, "也可以添加str");

//        for (int i = 0; i < integers.size(); i++) {
//            System.out.println(integers.get(i));
//        }
            integers.forEach(value-> System.out.println(value));

    }
}
