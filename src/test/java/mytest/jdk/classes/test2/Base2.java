package mytest.jdk.classes.test2;

/**
 * @version 1.0
 * @ClassName Base
 * @Description todo
 * @Author wangdi
 * @Date 2021/11/25 20:08
 **/

public class Base2 {
    private String baseName = "base";

    public Base2() {
        callName();
    }

    public void callName() {
        System.out.println(baseName);
    }
}
