package mytest.jdk.classes.test2;

/**
 * @version 1.0
 * @ClassName Sub
 * @Description todo
 * @Author wangdi
 * @Date 2021/11/25 20:09
 **/

public class Sub extends Base2 {
    private String baseName = "sub";

    @Override
    public void callName() {
        System.out.println(baseName);
    }
}
