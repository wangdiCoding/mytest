package mytest.jdk.classes.test1;

/**
 * @version 1.0
 * @ClassName Base
 * @Description todo
 * @Author wangdi
 * @Date 2021/11/25 19:59
 **/

public class Base {
    private String baseName = "base";

    public Base() {
        callName();
    }

    public void callName() {
        System.out.println(baseName);
    }

    public static class Sub extends Base {
        private String baseName = "sub";

        @Override
        public void callName() {
            System.out.println(baseName);
        }
    }
}
