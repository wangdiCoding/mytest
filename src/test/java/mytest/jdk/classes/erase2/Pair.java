package mytest.jdk.classes.erase2;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/10/19 17:00
 * @Modified By:
 */
class Pair<T> {

    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}