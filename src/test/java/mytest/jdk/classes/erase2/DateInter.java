package mytest.jdk.classes.erase2;

import java.util.Date;

/**
 * @Author dwang
 * @Company neldtv
 * @Description TODO
 * @create 2021/10/19 17:00
 * @Modified By:
 */
class DateInter extends Pair<Date> {

    @Override
    public void setValue(Date value) {
        super.setValue(value);
    }

    @Override
    public Date getValue() {
        return super.getValue();
    }

    public static void main(String[] args) throws ClassNotFoundException {
        DateInter dateInter = new DateInter();
        dateInter.setValue(new Date());
//        dateInter.setValue(new Object()); //编译错误
    }

//
//    @Override
//    public void setValue(Object value) {
//        setValue(value);
//    }
//
//    @Override
//    public Object getValue() {
//        getValue();
//    }
}
