package mytest.jdk.classes;

import mytest.jdk.classes.test1.Base;
import mytest.jdk.classes.test2.Base2;
import mytest.jdk.classes.test2.Sub;
import org.junit.jupiter.api.Test;

/**
 * @version 1.0
 * @ClassName ClassTest
 * @Description todo
 * @Author wangdi
 * @Date 2021/11/25 19:59
 **/

public class ClassTest {
    @Test
    void test1() {
        Base b = new Base.Sub();
    }

    @Test
    void test2() {
        Sub sub = new Sub();
    }
}
