package mytest.jdk.classes.classLoad;

/**
 * @Author dwang
 * @Description 静态内部类单例
 * @create 2022/1/17 17:54
 * @Modified By:
 */
public class OuterClass2 {

    public static void main(String[] args) {

        // 调用静态内部类的成员变量已经创建了外部类实例
        long innerStaticDate = OuterClass.InnerStaticClass.INNER_STATIC_DATE;

//        OuterClass.getInstance();


    }
}
