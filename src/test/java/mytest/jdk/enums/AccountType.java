package mytest.jdk.enums;

/**
 * @Author dwang
 * @Description 测试到底会调用几次构造方法
 * @create 2021/11/19 9:00
 * @Modified By:
 */
public enum AccountType {
    SAVING,FIXED,CURRENT;

    AccountType() {
        System.out.println("create function!");
    }

}
