package mytest.jdk.enums;

import org.junit.jupiter.api.Test;

/**
 * @Author dwang
 * @Description 测试会调用几次构造函数
 * @create 2021/11/19 9:01
 * @Modified By:
 */
public class EnumEqualCreateTest {
//    @Test
//    void print() {
//        AccountType fixed = AccountType.FIXED;
//        System.out.println(fixed);
//    }

    public static void main(String[] args) {
        AccountType fixed = AccountType.FIXED;
        System.out.println(fixed);
    }
}
