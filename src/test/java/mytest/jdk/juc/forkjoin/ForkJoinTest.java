package mytest.jdk.juc.forkjoin;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.concurrent.*;

/**
 * @Author dwang
 * @Description fork join线程池的初步demo
 * @create 2022/2/24 14:17
 * @Modified By:
 */
public class ForkJoinTest {

    public static final String BASE = "进行一次拆分, start: %s, mid: %s, end: %s";
    public static final String BASE2 = "start: %s,  end: %s";

    @Test
    public void test_demo() throws ExecutionException, InterruptedException {

        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinTask<Integer> task = new SumTask(1, 10000);
        forkJoinPool.submit(task);
        System.out.println("sum result = " + task.get());
    }

    static class SumTask extends RecursiveTask<Integer> {
        private int start;
        private int end;

        public SumTask(int start, int end) {
            this.start = start;
            this.end = end;
        }

        /**
         * The main computation performed by this task.
         * @return the result of the computation
         */
        @Override
        protected Integer compute() {
            if (end - start < 1000) {
                int sum = 0;
                String format = String.format(BASE2, start, end);
                System.out.println(format);

                for (int i = start; i <= end ; i++) {
                    sum += i;
                }
                return sum;
            }

            // 大于1000则拆分两个任务
            int midNum = (end + start) / 2;
            SumTask subTask1 = new SumTask(start, midNum);
            SumTask subTask2 = new SumTask(midNum + 1, end);
            String format = String.format(BASE, start, midNum, end);
//            System.out.println(format);
            subTask1.fork();
            subTask2.fork();

            return subTask1.join() + subTask2.join();

        }
    }

    @Test
    public void test_demo2() throws ExecutionException, InterruptedException {

        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
        ForkJoinTask<Integer> task = new SumTask(1, 10);
        forkJoinPool.submit(task);
        Integer integer = task.get();
        System.out.println("sum result = " + integer);
    }

    /**
     * fork join使用到的地方
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void test_demo3()  {
        long []arr = {1,2,3,5,1,5,213,11,5,1,6,14,5,43,5645,6757,7,44,2345,345,4,55,47,576,};
        System.out.println(Arrays.toString(arr));
        Arrays.parallelSort(arr);
        System.out.println(Arrays.toString(arr));

    }

    /**
     * ExecuteService创建fjpool
     */
    @Test
    public void test_demo4() throws ExecutionException, InterruptedException {
        ForkJoinPool executorService = (ForkJoinPool) Executors.newWorkStealingPool();
        ForkJoinTask<Integer> task = new SumTask(1, 10);
        executorService.submit(task);
        Integer integer = task.get();
        System.out.println("sum result = " + integer);
    }
}
