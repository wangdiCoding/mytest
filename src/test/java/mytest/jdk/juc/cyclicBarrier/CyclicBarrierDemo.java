package mytest.jdk.juc.cyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author dwang
 * @Description cyclic 简单使用。
 * 一次性的阻挡？
 * @create 2022/2/25 15:00
 * @Modified By:
 */



public class CyclicBarrierDemo {

    public static void main(String[] args) {

        CyclicBarrier cb = new CyclicBarrier(2, new Runnable() {
            @Override
            public void run() {
                System.out.println("--------------------------" + Thread.currentThread().getName() + "（barrier action）-------------------------------");
            }
        });

        CyclicThread t1 = new CyclicThread("t1", cb);
        CyclicThread t2 = new CyclicThread("t2", cb);

        t1.start();
        t2.start();
        cyclicWait(cb);
        cyclicWait(cb);

    }

    /**
     * print
     * @param cb
     */
    public static void cyclicWait(CyclicBarrier cb){
        System.out.println(Thread.currentThread().getName() + " going to await ");
        try {
            cb.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " continue ");
    }


    static class CyclicThread extends Thread {
        private CyclicBarrier cb;
        public CyclicThread(String name, CyclicBarrier cb) {
            super(name);
            this.cb = cb;
        }
        @Override
        public void run() {
            CyclicBarrierDemo.cyclicWait(cb);
//            CyclicBarrierDemo.cyclicWait(cb);
        }
    }
}
