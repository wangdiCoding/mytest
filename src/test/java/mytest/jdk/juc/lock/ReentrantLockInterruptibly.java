package mytest.jdk.juc.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author dwang
 * @Description 测试锁的获取，在可中断和不可以中断的结果
 * @create 2022/9/8 18:21
 * @Modified By:
 */
public class ReentrantLockInterruptibly {


    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();

        new Thread(new Runnable() {
            @Override
            public void run() {
                reentrantLock.lock();

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                reentrantLock.unlock();
            }
        }).start();


//        getLock(reentrantLock);

        //会被中断获取不到锁
        getlockError(reentrantLock);









    }

    private static void getlockError(ReentrantLock reentrantLock) {
        Thread getlockThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    reentrantLock.lockInterruptibly();
                    System.out.println("get lock success");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                reentrantLock.unlock();
            }
        });

        getlockThread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 中断thread获取锁的动作
        getlockThread.interrupt();
    }

    private static void getLock(ReentrantLock reentrantLock) {
        Thread canGetLockThread = new Thread(new Runnable() {
            @Override
            public void run() {
                reentrantLock.lock();
                System.out.println("get lock success");

                reentrantLock.unlock();
            }
        });

        canGetLockThread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 中断thread获取锁的动作
        canGetLockThread.interrupt();
    }

}
