package mytest.jdk.juc.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author dwang
 * @Description 公平锁测试
 * @create 2022/2/18 15:17
 * @Modified By:
 */
public class FairEnentrantLockTest {
    static class MyThread extends Thread {
        private Lock lock;

        public MyThread(String name, Lock lock) {
            super(name);
            this.lock = lock;
        }

        @Override
        public void run() {
            lock.lock();


            try {
                System.out.println(Thread.currentThread().getName() + " is running");

                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock(true);

        MyThread t1 = new MyThread("t1", reentrantLock);
        MyThread t2 = new MyThread("t2", reentrantLock);
        MyThread t3 = new MyThread("t3", reentrantLock);
        t1.start();
        t2.start();
        t3.start();

    }
}
