package mytest.jdk.juc.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author dwang
 * @Description 条件锁测试 --- 三个线程交替打印aaa 111 !!!
 * @create 2022/9/7 18:30
 * @Modified By:
 */
public class ReentrantLockConditionTest {


    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition conditionA = reentrantLock.newCondition();
        Condition conditionB = reentrantLock.newCondition();
        Condition conditionC = reentrantLock.newCondition();


        new Thread(new Runnable() {
            @Override
            public void run() {
//                while (true) {
                   printInfo(reentrantLock, conditionA, conditionB, "AAA");
//                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
//                while (true) {
                    printInfo(reentrantLock, conditionB, conditionA, "CCC");
//                }
            }
        }).start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    printInfo(reentrantLock, conditionB, conditionC, "BBB");
//
//                }
//            }
//        }).start();
    }

    private static void printInfo(ReentrantLock reentrantLock, Condition condition1, Condition condition2, String printInfo) {
        reentrantLock.lock();
        try {
            System.out.println("get lock " + printInfo);
            condition2.signal();
            System.out.println(printInfo);

            condition1.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("unlock " + printInfo);

        reentrantLock.unlock();
    }

}
