package mytest.jdk.juc.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author dwang
 * @Description 研究AQS流程使用
 * @create 2022/9/7 18:30
 * @Modified By:
 */
public class ReentrantLockTest {



    public static void main(String[] args) {

        ReentrantLock reentrantLock = new ReentrantLock();


        new Thread(new Runnable() {
            @Override
            public void run() {
                reentrantLock.lock();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                reentrantLock.unlock();
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                reentrantLock.lock();

                reentrantLock.unlock();

            }
        }).start();
    }
}
