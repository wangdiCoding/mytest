package mytest.jdk.juc.lock;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

/**
 * @Author dwang
 * @Description 展示LockSupport的使用
 * @create 2022/2/17 14:44
 * @Modified By:
 */
public class FIFOMutex {

    // 表示是否获取锁
    private final AtomicBoolean locked = new AtomicBoolean(false);

    private final Queue<Thread> waiters = new ConcurrentLinkedDeque<>();

    public void locked() {
        boolean wasInterrupted = false;
        Thread currentThread = Thread.currentThread();
        waiters.add(currentThread);
        System.out.println("thread name: " + currentThread.getName());
        // 如果队头不是当前线程 ,或者当前线程没有获取锁，则wait在这
        while (waiters.peek() != currentThread || !locked.compareAndSet(false, true)) {
            LockSupport.park(this);
            if (Thread.interrupted()) {
                wasInterrupted = true;
            }
        }
        Thread remove = waiters.remove();
        if (wasInterrupted) {
            currentThread.interrupt();
        }

    }

    public void unlock(){
        locked.set(false);
        LockSupport.unpark(waiters.peek());
    }

    public static void main(String[] args) throws InterruptedException {
        FIFOMutex fifoMutex = new FIFOMutex();
        new Thread(new Runnable() {
            @Override
            public void run() {
                fifoMutex.locked();
                fifoMutex.unlock();

                System.out.println("thread end");
            }
        }).start();

        // 睡眠就不会发生死锁 为了线程里面先unlock外面再unlock
        Thread.sleep(1000);
        fifoMutex.unlock();

        System.out.println("end");
        fifoMutex.locked();
    }
}
