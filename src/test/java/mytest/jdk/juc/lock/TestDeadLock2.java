package mytest.jdk.juc.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author dwang
 * @Description
 * @create 2022/12/8 11:08
 * @Modified By:
 */
class MyNode {

    MyNode parent;
    List<MyNode> child = new CopyOnWriteArrayList<>();

    //两件事，添加父类，在父类的子类里添加当前Node
    public synchronized void addParent(MyNode parentNode) {
        addParentOnly(parentNode);
        // 父类添加当前元素
        parentNode.addChildOnly(this);

    }

    public synchronized void addParentOnly(MyNode parentNode) {
        this.parent = parentNode;
    }


    //两件事，当前元素添加子类，子类添加当前元素
    public synchronized void addChild(MyNode childNode) {
        if (!this.child.contains(childNode)) {
            addChildOnly(childNode);
            childNode.addParentOnly(this);
        }
    }

    public void addChildOnly(MyNode childNode) {
        if (!this.child.contains(childNode)) {

            this.child.add(childNode);
        }
    }

}


public class TestDeadLock2 {

    // 如果线程1调用parent.addChild(child)方法的同时有
    // 另外一个线程2调用child.setParent(parent)方法，两个线程中的parent表示的是同一个对象，child亦然，
    // 此时就会发生死锁。下面的伪代码说明了这个过程：
    // 实际上并不能 不sleep死锁
    public static void main(String[] args) {
        MyNode parent = new MyNode();
        MyNode child = new MyNode();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("T1S");
                parent.addChild(child);
                child.addParentOnly(parent);
                System.out.println("T1E");
            }
        });
        t1.start();


        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("T2S");
                child.addParent(parent);
                parent.addChildOnly(child);
                System.out.println("T2E");
            }
        });
        t2.start();
    }


}
