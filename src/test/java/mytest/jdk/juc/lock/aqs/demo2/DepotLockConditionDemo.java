package mytest.jdk.juc.lock.aqs.demo2;

/**
 * @Author dwang
 * @Description 可重入锁条件下的学习AQS过程
 * @create 2022/2/17 17:29
 * @Modified By:
 */

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DepotLockConditionDemo {
    private int size;
    private int capacity;
    private Lock lock;
    private Condition fullCondition;
    private Condition emptyCondition;

    public DepotLockConditionDemo(int capacity) {
        this.capacity = capacity;
        lock = new ReentrantLock();
        fullCondition = lock.newCondition();
        emptyCondition = lock.newCondition();
    }

    /**
     * size 是剩下的
     * left 是放进来的
     * @param no
     */
    public void produce(int no) {
        lock.lock();
        int left = no;
        try {
            while (left > 0) {
                while (size >= capacity)  {
                    System.out.println(Thread.currentThread() + " fullCondition  before await");
                    fullCondition.await();
                    System.out.println(Thread.currentThread() + " fullCondition after await");
                }
                // 计算添加的最大量
                int inc = (left + size) > capacity ? (capacity - size) : left;
                left -= inc;
                size += inc;
                System.out.println(Thread.currentThread() + " 生产者成功生产了！，produce = " + inc + ", size = " + size);
                emptyCondition.signal();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void consume(int no) {
        lock.lock();
        int left = no;
        try {
            while (left > 0) {
                while (size <= 0) {
                    System.out.println(Thread.currentThread() + " emptyCondition before await");
                    emptyCondition.await();
                    System.out.println(Thread.currentThread() + " emptyCondition after await");
                }
                int dec = (size - left) > 0 ? left : size;
                left -= dec;
                size -= dec;
                System.out.println(Thread.currentThread() + " 消费者成功消费了， consume = " + dec + ", size = " + size);
                fullCondition.signal();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
