package mytest.jdk.juc.lock.aqs.demo1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author dwang
 * @Description 学习AQS的具体运行机制：简单的无条件的获取锁过程
 * @create 2022/2/17 16:44
 * @Modified By:
 */
public class AbstractQueuedSynchonizerDemo {

    public static void main(String[] args) {

        Lock lock = new ReentrantLock();

        MyThread t1 = new MyThread("t1", lock);
        MyThread t2 = new MyThread("t2", lock);
        t1.start();
        t2.start();

    }

    static class MyThread extends Thread{
        private Lock lock;
        public MyThread(String name, Lock lock) {
            super(name);
            this.lock = lock;
        }

        public void run () {
            lock.lock();
            try {
                System.out.println(Thread.currentThread() + " running");
            } finally {
                lock.unlock();
            }
        }
    }

}
