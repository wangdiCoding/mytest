package mytest.jdk.juc.lock.aqs.demo2;

/**
 * @Author dwang
 * @Description 条件锁的使用 - 生产消费模式
 * @create 2022/2/17 17:29
 * @Modified By:
 */
public class AQS2 {

    static class Consumer {
        private DepotLockConditionDemo depotLockConditionDemo;
        public Consumer(DepotLockConditionDemo depotLockConditionDemo) {
            this.depotLockConditionDemo = depotLockConditionDemo;
        }
        public void consume(int no) {
            new Thread(() -> depotLockConditionDemo.consume(no), no + " consume thread").start();
        }
    }

   static class Producer {
        private DepotLockConditionDemo depotLockConditionDemo;
        public Producer(DepotLockConditionDemo depotLockConditionDemo) {
            this.depotLockConditionDemo = depotLockConditionDemo;
        }
        public void produce(int no) {
            new Thread(() -> depotLockConditionDemo.produce(no), no + " produce thread").start();
        }
    }

    /**
     * 测试等待消费了才会生成新的，生产达到上限之后
     * 这里就是同一个线程，所以获取到了锁
     *
     * @param args
     */
    public static void main(String[] args) {

        DepotLockConditionDemo depotLockConditionDemo = new DepotLockConditionDemo(500);
        Producer p1 = new Producer(depotLockConditionDemo);
        p1.produce(500);
        Producer p2 = new Producer(depotLockConditionDemo);
        p2.produce(200);
        Consumer c1 = new Consumer(depotLockConditionDemo);
        c1.consume(500);
        Consumer c2 = new Consumer(depotLockConditionDemo);
        c2.consume(200);

    }
}
