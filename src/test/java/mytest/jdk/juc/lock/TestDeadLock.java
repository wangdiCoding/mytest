package mytest.jdk.juc.lock;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.CountDownLatch;

/**
 * @Author dwang
 * @Description 1. 实现一段一定会出现死锁的代码，不使用while，for，sleep等硬等待；
 * @create 2022/12/8 10:36
 * @Modified By:
 */
public class TestDeadLock {


    /**
     * 并不会发生死锁，而是t1获取不到资源resource2被阻塞，g2就是被park了
     */
    public void test_1() {
        Object resource1 = new Object();
        Object resource2 = new Object();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);

        new Thread(new Runnable() {
            @Override
            public void run() {

                synchronized (resource1) {
                    countDownLatch.countDown();
                    //1

                    try {
                        countDownLatch2.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (resource2) {
                        //2
                    }

                }
            }
        }, "t1").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resource2) {
                    //3


                    synchronized (resource1) {
                        //4
                        countDownLatch.countDown();

                    }

                }
            }
        }, "g2").start();

        //期望的执行顺序是
        //1 - 3 - 2 - 4
    }

    /**
     * 把countdownlatch换成sleep就死锁了
     */
    @Test
    public void test2_sleep() {
        Object resource1 = new Object();
        Object resource2 = new Object();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource1) {
                    //1
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (resource2) {
                        //2

                    }
                }
            }
        }, "t1").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource2) {
                    //3
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (resource1) {
                        //4


                    }
                }
            }
        }, "g2").start();

        //期望的执行顺序是
        //1 - 3 - 2 - 4
    }


    public static void test3_wait() {
        Object resource1 = new Object();
        Object resource2 = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource1) {
                    //1
                    try {
                        resource1.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (resource2) {
                        //2
                    }
                }
            }
        }, "t1").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource2) {
                    //3
                    try {
                        resource2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (resource1) {
                        //4
                    }
                }
            }
        }, "g2").start();
    }

    /**
     * 模拟耗时操作
     */
    public static void test4_longTime() {
        Object resource1 = new Object();
        Object resource2 = new Object();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource1) {
                    //1
                    try {
                        getConnect();
                    } catch (IOException e) {
//                        e.printStackTrace();
                    }
                    synchronized (resource2) {
                        //2
                    }
                }
            }
        }, "t1").start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resource2) {
                    //3
                    try {
                        getConnect();
                    } catch (IOException e) {
                    // e.printStackTrace();
                    }

                    synchronized (resource1) {
                        //4
                    }
                }
            }
        }, "g2").start();

        //期望的执行顺序是
        //1 - 3 - 2 - 4
    }

    public static void getConnect() throws IOException {
        String path = "https://www.google.com";
        //网络协议对象（远程对象）
        HttpURLConnection urlConnection = null;
        //定位网络资源
        URL url = new URL(path);
        //调用url的openConnection（）连接远程对象
        urlConnection = (HttpURLConnection) url.openConnection();
        //发送连接请求，实现真正的连接
        urlConnection.connect();
    }


    public static void main(String[] args) {
//        test2_sleep();
        test3_wait();
//        test4_longTime();
    }
}
