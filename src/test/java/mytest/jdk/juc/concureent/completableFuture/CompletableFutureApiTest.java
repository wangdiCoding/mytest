package mytest.jdk.juc.concureent.completableFuture;

import org.junit.jupiter.api.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * @Author dwang
 * @Description api测试
 * @create 2022/3/2 11:49
 * @Modified By:
 */
public class CompletableFutureApiTest {

    /**
     * 创建CompletableFuture对象。
     */
    @Test
    public void demo1() throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            //长时间的计算任务
            return "·00";
        });

        String result = future.get();
        System.out.println(result);

    }

    /**
     * 计算结果完成时的处理
     * whenComplete
     * exceptionally
     * handle
     *
     */
    @Test
    public void demo2() throws ExecutionException, InterruptedException, IOException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(CompletableFutureApiTest::getMoreData);
        Future<Integer> f = future.whenComplete((v, e) -> {
            System.out.println("v = " + v);
            System.out.println("e = " + e);
        });

        CompletableFuture<Integer> exceptionally = future.exceptionally((e) -> {
            System.out.println("exceptionally!!!!!!  e = " + e);
            return -1;

        });

//        System.out.println(future.get());
//        // 阻塞
//        System.in.read();


        CompletableFuture<String> handle = future.handle((integer, throwable) -> {
            System.out.println("result " + integer);
            return "偷天换日";
        });
        String s = handle.get();
        System.out.println(s);

    }

    private static Random rand = new Random();
    private static long t = System.currentTimeMillis();

    static int getMoreData() {
        System.out.println("begin to start compute");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("end to start compute. passed " + (System.currentTimeMillis() - t) / 1000 + " seconds");
        int i = 1 / 0;
        return rand.nextInt(1000);
    }


    @Test
    public void demo_handle() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });

        CompletableFuture<String> handle = future.handle(new BiFunction<Integer, Throwable, String>() {
            @Override
            public String apply(Integer integer, Throwable throwable) {
                return integer * 10 + "";

            }
        });
        System.out.println(handle.get());
    }

    /**
     * 转换,相当于管道处理 ， 一步一步 apply
     */
    @Test
    public void demo3_apply() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });
        CompletableFuture<String> f = future.thenApplyAsync(i -> i * 10).thenApply(i -> i.toString());
        System.out.println(f.get()); //"1000
    }

    /**
     * 只消费结果
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void demo4_accept() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });
        CompletableFuture<Void> f = future.thenAccept(System.out::println);
        System.out.println(f.get());
    }

    /**
     * 只消费结果
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void demo4_accept_both() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });
        CompletableFuture<Void> f = future.thenAcceptBoth(CompletableFuture.completedFuture(10), (x, y) -> System.out.println(x * y));
        System.out.println(f.get());
    }


    /**
     * 纯消费 - 但是消费的是Runable。 上面accept消费的是Consumer
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void demo4_run() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });
        CompletableFuture<Void> f = future.thenRun(() -> System.out.println("finished"));
        // 会丢弃之前的结果
        System.out.println(f.get());
    }


    /**
     * 这一组方法接受一个Function作为参数，这个Function的输入是当前的CompletableFuture的计算值，
     * 返回结果将是一个新的CompletableFuture，这个新的CompletableFuture会组合原来的CompletableFuture
     * 和函数返回的CompletableFuture。
     *
     * 感觉和handle差不多
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void demo_compose() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });
        CompletableFuture<String> f = future.thenCompose(i -> {
            return CompletableFuture.supplyAsync(() -> {
                return (i * 10) + "";
            });
        });
        System.out.println(f.get()); //1000
    }


    @Test
    public void demo_combine() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return 100;
            }
        });
        CompletableFuture<String> stringCompletableFuture = CompletableFuture.supplyAsync(new Supplier<String>() {

            /**
             * Gets a result.
             *
             * @return a result
             */
            @Override
            public String get() {
                return "abc";
            }
        });

        CompletableFuture<Map> mapCompletableFuture = integerCompletableFuture.thenCombine(stringCompletableFuture, new BiFunction<Integer, String, Map>() {
            @Override
            public Map apply(Integer integer, String s) {
                HashMap<Integer, String> integerStringHashMap = new HashMap<>();
                integerStringHashMap.put(integer, s);
                return integerStringHashMap;
            }
        });

        Map map = mapCompletableFuture.get();
        System.out.println(map);
    }


    /**
     * thenAcceptBoth和runAfterBoth是当两个CompletableFuture都计算完成，
     * 而我们下面要了解的方法是当任意一个CompletableFuture计算完成的时候就会执行。
     */
    @Test
    public void demo_either() throws ExecutionException, InterruptedException {
        Random rand = new Random();
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(10000 + rand.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 100;
        });
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(10000 + rand.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 200;
        });
        CompletableFuture<String> f = future.applyToEither(future2, i -> i.toString());

        System.out.println("f = " + f.get());


    }

    @Test
    public void show_run_time() throws ExecutionException, InterruptedException {

        long start = System.nanoTime();
//        demo_handle();
        demo_compose();


        long end = System.nanoTime();

        System.out.println("run time:  " + (end - start) + " (ns)");

    }

    @Test
    public void completeable_task_question() {
        CompletableFuture task1 = CompletableFuture.runAsync(() -> {
            /*** 这里会调用一个RPC请求，而这个RPC请求处理的过程中会通过SPL机制load指定接口的实现，这个接口所在jar存在于WEB-INFO/lib */
//            ArrayList list = new ArrayList();
//            ServiceLoader serviceLoader = ServiceLoader.load(xxx interface);
//            serviceLoader.forEach(xxx->{ list.add(xxx)});
            System.out.println("任务1执行");
        });
        CompletableFuture task2 = CompletableFuture.runAsync(() -> {
            System.out.println("任务2执行");
        });
        CompletableFuture task3 = CompletableFuture.runAsync(() -> {
            System.out.println("任务3执行");
        });        // 等待所以任务执行完成返回        CompletableFuture.allOf(task1,task2,task3).join();        return result;

    }


}
