package mytest.jdk.juc.concureent;

import mytest.jdk.collection.map.MapKey;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author dwang
 * @Description 查看CurrentHashMap如何保证线程安全的
 * @create 2022/2/16 14:02
 * @Modified By:
 */
public class CurrentHashMapTest {

    /**
     * 看怎么保证线程安全的
     */
    @Test
    public void putTest() {

        ConcurrentHashMap<MapKey, String> ConcurrentHashMap = new ConcurrentHashMap<>();
        String value = "mapValue";

        // 第一阶段测试
        for (int i = 0; i < 6; i++) {
            ConcurrentHashMap.put(new MapKey(String.valueOf(i)), value);
        }
        System.out.println(ConcurrentHashMap);

        // 第二阶段测试 ，添加会发生扩容(不需要12次（threshold）)
        ConcurrentHashMap<MapKey, String> hashMap2 = new ConcurrentHashMap<>();
        for (int i = 0; i < 10; i++) {
            hashMap2.put(new MapKey(String.valueOf(i)), value);
        }
        System.out.println(hashMap2);

        // 再次扩容因为50 > 48
        ConcurrentHashMap<MapKey, String> hashMap3 = new ConcurrentHashMap<>();
        for (int i = 0; i < 50; i++) {
            hashMap3.put(new MapKey(String.valueOf(i)), value);
        }
        System.out.println(hashMap3);

    }

    @Test
    public void put_if_present() {

        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();

        String key = concurrentHashMap.putIfAbsent("key", "value");
        System.out.println(key);
        String key2 = concurrentHashMap.putIfAbsent("key", "value");
        System.out.println(key2);


    }
}
