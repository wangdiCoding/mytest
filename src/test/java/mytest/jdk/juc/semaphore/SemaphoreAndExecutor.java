package mytest.jdk.juc.semaphore;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Semaphore;

/**
 * @Author dwang
 * @Description 测试在不知道有多少个任务的情况下实现最后完成
 * @create 2022/11/21 14:43
 * @Modified By:
 */
public class SemaphoreAndExecutor {

    /**
     * 可以实现，但是还是需要主线程主观等待
     * @throws InterruptedException
     */
    @Test
    void semaphore_test() throws InterruptedException {

        Semaphore semaphore = new Semaphore(0);

        //外面+1
        semaphore.release();
        semaphore.release();
        //里面-1
        semaphore.acquire();
        semaphore.acquire();

        // 能拿到则表示不能结束
        while (semaphore.tryAcquire()) {
            System.out.println("还没结束");
        }

        System.out.println("success");
    }


    @Test
    void semaphore_test2() throws InterruptedException {
        Semaphore semaphore = new Semaphore(Integer.MAX_VALUE);



        //外面+1
        semaphore.acquire();
        semaphore.acquire();


        //里面-1
        semaphore.release();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        // 同时拿初始量，如果不结束就拿不到
        semaphore.acquire(Integer.MAX_VALUE);

        System.out.println("success");
    }
}
