package mytest.jdk.juc.countdownlatch;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * @Author dwang
 * @Description 实现一个容器，提供两个方法，add，size 写两个线程，线程1添加10个元素到容器中，
 * 线程2实现监控元素的个数，当个数到5个时，线程2给出提示并结束
 * @create 2022/2/25 16:02
 * @Modified By:
 */
public class CountDownLatchTest2 {

    public static ArrayList<Integer> integers = new ArrayList<>();
    private static CountDownLatch countDownLatch = new CountDownLatch(1);


    public static void main(String[] args) {
        AddThread t1 = new AddThread("t1", countDownLatch);
        TipThread t2 = new TipThread("t2", countDownLatch);
        t1.start();
        t2.start();
    }
}



class TipThread extends Thread {
    private CountDownLatch countDownLatch;

    public TipThread(String name, CountDownLatch countDownLatch) {
        super(name);
        this.countDownLatch = countDownLatch;
    }
    @Override
    public void run() {
        try {
            countDownLatch.await();
            System.out.println("已添加5个: 但是size=" + CountDownLatchTest2.integers.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class AddThread extends Thread {
    private CountDownLatch countDownLatch;

    public AddThread(String name, CountDownLatch countDownLatch) {
        super(name);
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            CountDownLatchTest2.integers.add(i);
            if (i == 4) {
                countDownLatch.countDown();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("add end");
    }
}