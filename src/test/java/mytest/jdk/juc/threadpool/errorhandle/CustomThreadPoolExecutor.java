package mytest.jdk.juc.threadpool.errorhandle;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author dwang
 * @Description 线程池
 * @create 2023/5/6 10:35
 * @Modified By:
 */
public class CustomThreadPoolExecutor extends ThreadPoolExecutor {

    public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);

        if (t != null) {
            System.out.println("处理异常");
            ErrorRunnable errorRunnable = (ErrorRunnable) r;

            Bank bank = errorRunnable.getBank();
            bank.a = 1000;
            bank.b = 1000;

        }
    }
}
