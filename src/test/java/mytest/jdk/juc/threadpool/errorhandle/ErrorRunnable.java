package mytest.jdk.juc.threadpool.errorhandle;

/**
 * @Author dwang
 * @Description TODO
 * @create 2023/5/4 18:15
 * @Modified By:
 */
public class ErrorRunnable implements Runnable {
    private Bank bank;

    public ErrorRunnable(Bank bank) {
        this.bank = bank;
    }
    @Override
    public void run() {
        bank.transfer();
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}
