package mytest.jdk.juc.threadpool.errorhandle;

/**
 * @Author dwang
 * @Description 记录ab两个人的钱
 * @create 2023/5/5 11:36
 * @Modified By:
 */
public class Bank {
    public int a = 1000;
    public int b = 1000;

    public void transfer() {
        a += 1000;
        //出现异常
        int i = 1 / 0;
        b -= 1000;
    }

//    public void transfer() {
//        try {
//            a += 1000;
//            //出现异常
//            int i = 1 / 0;
//            b -= 1000;
//        } catch (Exception e) {
//            e.printStackTrace();
//            a = 1000;
//            b = 1000;
//        }
//    }


    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
