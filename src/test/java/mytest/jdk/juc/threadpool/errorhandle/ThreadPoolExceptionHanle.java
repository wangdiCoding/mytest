package mytest.jdk.juc.threadpool.errorhandle;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.concurrent.*;


/**
 * @Author dwang
 * @Description 线程池中出现异常，线程池外并不能准确看到问题的地方
 * @create 2023/5/4 17:53
 * @Modified By:
 */
public class ThreadPoolExceptionHanle {

    public static void main(String[] args) {
        // try catch to reset
//        handle();

        // 自定义线程池
        hanle2();

    }

    private static void hanle2() {
        Bank bank = new Bank();
        ErrorRunnable errorRunnable = new ErrorRunnable(bank);

        ThreadPoolExecutor threadPoolExecutor = new CustomThreadPoolExecutor(10, 10, 120, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
        threadPoolExecutor.execute(errorRunnable);
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(bank);
        threadPoolExecutor.shutdown();
    }


    private static void handle() {
        Bank bank = new Bank();
        ErrorRunnable errorRunnable = new ErrorRunnable(bank);

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 10, 120, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
        threadPoolExecutor.execute(errorRunnable);
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(bank);
        threadPoolExecutor.shutdown();
    }




}
