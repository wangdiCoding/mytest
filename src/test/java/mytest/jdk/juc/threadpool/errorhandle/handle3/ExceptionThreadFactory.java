package mytest.jdk.juc.threadpool.errorhandle.handle3;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @Author dwang
 * @Description TODO
 * @create 2023/5/6 11:01
 * @Modified By:
 */
public class ExceptionThreadFactory implements ThreadFactory {

    ThreadFactory threadFactory = Executors.defaultThreadFactory();
    Thread.UncaughtExceptionHandler handler;

    public ExceptionThreadFactory(Thread.UncaughtExceptionHandler handler) {
        this.handler = handler;
    }

    /**
     * Constructs a new {@code Thread}.  Implementations may also initialize
     * priority, name, daemon status, {@code ThreadGroup}, etc.
     *
     * @param r a runnable to be executed by new thread instance
     * @return constructed thread, or {@code null} if the request to
     *         create a thread is rejected
     */
    @Override
    public Thread newThread(Runnable r) {
        Thread thread = threadFactory.newThread(r);
        thread.setUncaughtExceptionHandler(handler);
        return thread;
    }
}
