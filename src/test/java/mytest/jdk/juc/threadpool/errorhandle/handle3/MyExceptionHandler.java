package mytest.jdk.juc.threadpool.errorhandle.handle3;

/**
 * @Author dwang
 * @Description TODO
 * @create 2023/5/6 10:57
 * @Modified By:
 */
public class MyExceptionHandler implements Thread.UncaughtExceptionHandler {
    /**
     * Method invoked when the given thread terminates due to the
     * given uncaught exception.
     * <p>Any exception thrown by this method will be ignored by the
     * Java Virtual Machine.
     * @param t the thread
     * @param e the exception
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("处理异常");

        //然后呢？怎么处理

    }
}
