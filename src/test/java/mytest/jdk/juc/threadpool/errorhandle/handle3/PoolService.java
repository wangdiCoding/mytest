package mytest.jdk.juc.threadpool.errorhandle.handle3;

import mytest.jdk.juc.threadpool.errorhandle.Bank;
import mytest.jdk.juc.threadpool.errorhandle.ErrorRunnable;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author dwang
 * @Description TODO
 * @create 2023/5/6 10:57
 * @Modified By:
 */
public class PoolService {

    public static void main(String[] args) {
        ExceptionThreadFactory factory = new ExceptionThreadFactory(new MyExceptionHandler());
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 10, 120, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10), factory);
        Bank bank = new Bank();
        ErrorRunnable errorRunnable = new ErrorRunnable(bank);
        threadPoolExecutor.execute(errorRunnable);

        threadPoolExecutor.shutdown();
    }
}
