package mytest.jdk.juc.threadpool;

import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/2/23 15:48
 * @Modified By:
 */

import java.util.concurrent.ArrayBlockingQueue;
        import java.util.concurrent.Executors;
        import java.util.concurrent.ThreadFactory;
        import java.util.concurrent.ThreadPoolExecutor;
        import java.util.concurrent.TimeUnit;


public class ThreadpoolTest {

    @Test
    public void test_execute() throws InterruptedException {
        //RejectedExecutionHandler implementation
        //Get the ThreadFactory implementation to use
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        //creating the ThreadPoolExecutor
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(2, 8, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(2), threadFactory);
        //start the monitoring thread
        MyMonitorThread monitor = new MyMonitorThread(executorPool, 3);
        Thread monitorThread = new Thread(monitor);
        monitorThread.start();
        //submit work to the thread pool
        for(int i=0; i<10; i++){
            executorPool.execute(new WorkThread("cmd"+i));
        }

        Thread.sleep(30000);
        //shut down the pool
        executorPool.shutdown();
        //shut down the monitor thread
        Thread.sleep(5000);
        monitor.shutdown();

    }


    @Test
    public void test_submit() throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> submit = executorService.submit(new Callable<String>() {
            /**
             * Computes a result, or throws an exception if unable to do so.
             *
             * @return computed result
             * @throws Exception if unable to compute a result
             */
            @Override
            public String call() throws Exception {
                TimeUnit.SECONDS.sleep(5);
                return "future result";
            }
        });

        try {
            String result = submit.get();
            System.out.println(result);

        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }


}
