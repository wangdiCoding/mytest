package mytest.jdk.juc.threadpool;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/2/23 15:47
 * @Modified By:
 */
public class WorkThread implements Runnable {

    private String command;

    public WorkThread(String s) {
        this.command = s;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Start. Command = " + command);
        processCommand();
        System.out.println(Thread.currentThread().getName() + " End.");
    }

    private void processCommand() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return this.command;
    }
}
