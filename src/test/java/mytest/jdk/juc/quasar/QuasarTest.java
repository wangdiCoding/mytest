package mytest.jdk.juc.quasar;

import co.paralleluniverse.fibers.Fiber;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.SuspendableCallable;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author dwang
 * @Description 携程与线程池运行进行对比
 * @create 2022/2/24 16:05
 * @Modified By:
 */
public class QuasarTest {
    @Test
    public void thread_pool_test() throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(10000);
        long start = System.currentTimeMillis();
        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 10000; i++) {
            executor.submit(() -> {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        long end = System.currentTimeMillis();
        System.out.println("Thread use:" + (end - start) + " ms");
    }

    /**
     * 加上执行参数
     * -ea -javaagent:F:\myproject\mytest\lib\quasar-core-0.7.10.jar
     * @throws InterruptedException
     */
    @Test
    public void quasar_test() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(10000);
        long start = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            new Fiber<>(new SuspendableCallable<Object>() {
                @Override
                public Integer run() throws SuspendExecution, InterruptedException {
                    Fiber.sleep(1000);
                    countDownLatch.countDown();
                    return null;
                }
            }).start();
        }

        countDownLatch.await();
        long end = System.currentTimeMillis();
        System.out.println("Fiber use:" + (end - start) + "ms");

    }
}
