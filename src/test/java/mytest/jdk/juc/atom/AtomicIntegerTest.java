package mytest.jdk.juc.atom;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author dwang
 * @Description TODO
 * @create 2022/2/17 11:29
 * @Modified By:
 */
public class AtomicIntegerTest {
    static AtomicInteger atomicInteger;

    /**
     * 简单使用
     */
    @Test
    void testAtomicInteger(){
        AtomicInteger atomicInteger = new AtomicInteger();
        Integer num = 10000;

        for (int i = 0; i < num; i++) {
            System.out.println(atomicInteger.incrementAndGet());
            System.out.println(atomicInteger.getAndIncrement());
        }
    }

    /**
     * 有自旋操作,模拟会有添加失败的情况
     * @throws InterruptedException
     */
    @Test
    void test_mutiThread() throws InterruptedException {
        atomicInteger = new AtomicInteger();
        Integer num = 1000;
        CountDownLatch countDownLatch = new CountDownLatch(num);
        for (int i = 0; i < num; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 100; j++) {
                        int i1 = atomicInteger.get();

                        // cas 不保证都能加上
                        boolean b = atomicInteger.compareAndSet(i1, ++i1);
                        System.out.println(b);
                        // 自旋锁保证都能加上
//                        int i2 = atomicInteger.incrementAndGet();
                    }

                    countDownLatch.countDown();
                }
            }).start();
        }
        countDownLatch.await();


        System.out.println(atomicInteger.get());
        System.out.println("end");
    }

}
