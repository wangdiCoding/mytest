package mytest.jdk.juc.atom;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author dwang
 * @Description 引用类型更新
 * @create 2022/2/17 11:56
 * @Modified By:
 */
public class AtomicReferenceTest {

    @Test
    public void test_atom_reference() {

        User conan = new User("conan", 7);
        AtomicReference<User> userAtomicReference = new AtomicReference<>(conan);
        User gege = new User("giegie", 17);

        boolean b = userAtomicReference.compareAndSet(conan, gege);


        // 注意updater的创建
        AtomicIntegerFieldUpdater<User> age = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");
        Integer integer = age.get(conan);

    }


    @Test
    public void test_atom_reference2() {

        User conan = new User("conan", 7);
        AtomicReference<User> userAtomicReference = new AtomicReference<>(conan);
        User gege = new User("giegie", 17);

        userAtomicReference.getAndSet(gege);




        // 注意updater的创建
        AtomicIntegerFieldUpdater<User> age = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");
        Integer integer = age.get(conan);

    }

    static class User {
        private String name;

        // 需要public volatile
        public volatile  int age;

        public User() {
        }

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
