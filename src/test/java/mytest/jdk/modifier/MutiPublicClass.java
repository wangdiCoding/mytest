package mytest.jdk.modifier;

import org.junit.jupiter.api.Test;

/**
 * @Author dwang
 * @Description 实例化内部类
 * @create 2021/10/11 17:25
 * @Modified By:
 */
public class MutiPublicClass {

    @Test
    void test() {
        FatherClass fatherClass = new FatherClass();
        FatherClass.SonClass sonClass = fatherClass.new SonClass();

    }
}
