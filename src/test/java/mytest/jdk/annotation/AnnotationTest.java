package mytest.jdk.annotation;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

/**
 * @Author dwang
 * @Description 注解反射使用测试
 * @create 2022/1/20 18:31
 * @Modified By:
 */
public class AnnotationTest {


    @Test
    public void test() {

    }


    @LogAnnotation(logInfo = "show()，方法执行")
    public void show(){
        System.out.println("我在执行我的任务流程");
    }


}
