package mytest.jdk.string;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author dwang
 * @Description 正则匹配测试
 * @create 2022/4/14 10:25
 * @Modified By:
 */
public class MatchPatternTest {

    @Test
    public void match() {

        String exp = "≤0.5≤0.6≤0.7≤0.8≤-0.9≤0.99≤0.999≤0.9999≤0.99999≤0.999999";
        Pattern pattern = Pattern.compile("(-?\\d+)(\\.\\d+)?");
        Matcher matcher = pattern.matcher(exp);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}
