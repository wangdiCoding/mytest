package mytest.jdk.string;

/**
 * @version 1.0
 * @ClassName Main
 * @Description
 * 撤销/恢复操作具有广泛的用途，比如word文档中输入一个单词，可以点撤销，然后可以再恢复。
 * 编程实现如下功能：  从标准输入读取到一个字符串，字符串可包含0个或多个单词，单词以空格或者tab分隔； 如果遇到 "undo" 字符串，表示"撤销"操作，前一个字符串被撤销掉；
 * 如果遇到"redo"字符串，表示恢复刚才撤销掉的字符串.
 * 例如:   输入字符串 "hello undo redo world."，  对字符串中的 undo 和 redo 处理后， 最终输出的结果为 "hello world."
 * @Author wangdi
 * @Date 2022/2/23 22:17
 **/

public class Main {
    public static void main(String[] args) {
        String importWord = "hello undo redo world.";
        System.out.println("import: " + importWord);
        String result = getResult(importWord);
        System.out.println("outport: " + result);

    }
    public static String getResult(String input) {
        String[] s = input.split(" ");

        if (s.length == 2 && "undo".equals(s[1])) {
            s[0]="";
            s[1]="";
        }

        for (int i = 0; i < s.length; i++) {
            int preIndex = i;
            int pointIndex = i + 1;
            int nextIndex = i + 2;

            if (nextIndex >= s.length) {
                break;
            }
            if ("undo".equals(s[pointIndex]) && "redo".equals(s[nextIndex])){
                s[pointIndex]="";
                s[nextIndex]="";
            } else if ("undo".equals(s[pointIndex])) {
                s[preIndex]="";
                s[pointIndex]="";
            }
        }
        String out = "";
        for (String s1 : s) {
            if (s1 != "") {
                out +=s1;
                out +=" ";
            }
        }

        return out;
    }
}
