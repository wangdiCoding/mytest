package mytest.jdk.number;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @ClassName DoubleTest
 * @Author wangDi
 * @date 2020-12-22 10:16
 */
public class DoubleTest {

    public static void main(String[] args) {
        int interval = 240;
        for (int i = 0; i < 1000; i++) {
            int betweenMinutes = i;
            BigDecimal divide = null;
            try {
                divide = BigDecimal.valueOf(betweenMinutes).divide(BigDecimal.valueOf(interval), RoundingMode.UNNECESSARY);
                Double doubleValue = divide.doubleValue();
                int intValue = divide.intValue();
                if (doubleValue == intValue) {
                    System.out.print("betweenMinutes：" + i);
                    System.out.println("    doubleValue: " + doubleValue);
                }
            } catch (ArithmeticException e) {
            }
//            if (doubleValue.intValue() == doubleValue) {
//                System.out.print("betweenMinutes：" + i);
//                System.out.println("    doubleValue: " + doubleValue);
//            }



        }
    }

    /**
     * 直接除
     */
    @Test
    void test2 () {
        double interval = 240d;
//        for (int i = 0; i < 1000; i++) {
//            int betweenMinutes = i;
//
//            // 直接除保留整数部分
//            Double v = i / interval;
//            System.out.println(v);
//            if (v.intValue() == v) {
//                System.out.print("betweenMinutes：" + betweenMinutes);
//                System.out.println("    doubleValue: " + v);
//            }
//
//        }


        for (int i = 0; i < 1000; i++) {
            ImmutablePair<Boolean, Long> booleanLongImmutablePair = checkOrderOverTime(i, interval);
            if (booleanLongImmutablePair.getLeft()) {
                System.out.println(booleanLongImmutablePair.getRight());
            }

        }
    }



    private ImmutablePair<Boolean, Long> checkOrderOverTime(long betweenMinutes, Double interval) {

        if (betweenMinutes >= interval) {
//            Double doubleValue = BigDecimal.valueOf(betweenMinutes).divide(BigDecimal.valueOf(interval), 2, RoundingMode.HALF_UP).doubleValue();

            Double doubleValue = betweenMinutes / interval;
            if (doubleValue.intValue() == doubleValue) {
                return ImmutablePair.of(Boolean.TRUE, betweenMinutes);
            }
        }
        return ImmutablePair.of(Boolean.FALSE, null);

    }


    /**
     *
     */
    @Test
    public void test_random() {
        List<Triple<Integer, Integer, Integer>> list = new ArrayList<>();
        list.add(Triple.of(2049371,1,5));
        list.add(Triple.of(1672077,1,3));
        list.add(Triple.of(2049360,1,30));
        list.add(Triple.of(2023072,1,30));
        list.add(Triple.of(2435899,1,30));
        list.add(Triple.of(5062006,1,30));
        list.add(Triple.of(4021040,1,5));

        int itemid = 0;
        int count = 0;
        int p = 0;
        double bestValue = Double.MAX_VALUE;
        for (Triple<Integer, Integer, Integer> element : list) {
            double a = (Integer) element.getRight().intValue();
            double r = a / 10000.0D;
            double value = -Math.log(Randomizer.nextDouble()) / r;
            if (value < bestValue) {
                bestValue = value;
                itemid = ((Integer) element.getLeft()).intValue();
                count = ((Integer) element.getMiddle()).intValue();
                p = ((Integer) element.getRight()).intValue();
            }
        }

        System.out.println("itemid=" + itemid + "  count= " + count + " percent= " + p);

    }

}
