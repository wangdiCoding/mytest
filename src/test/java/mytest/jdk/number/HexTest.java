package mytest.jdk.number;

import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * @Author dwang
 * @Description TODO
 * @create 2021/11/25 17:24
 * @Modified By:
 */
public class HexTest {


    /**
     * 自增id转16进制数 用四个字符表示
     */
    @Test
    void testHex(){
        int id = 128;

        String format = String.format("%04X", id);
        System.out.println(format);


        byte[] bytes = integerTo2Bytes(id);
        String s = HexStringUtils.toHexString(bytes);

        String s1 = Arrays.toString(bytes);
        System.out.println(s);
        System.out.println(s1);

    }


    public static byte[] integerTo2Bytes(int value) {
        byte[] result = new byte[2];
        result[0] = (byte) ((value >>> 8) & 0xFF);
        result[1] = (byte) (value & 0xFF);
        return result;
    }
}
