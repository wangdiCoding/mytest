package mytest.jdk.number;

/** 十六进制字符串工具类 */
public class HexStringUtils {

  private static final char[] DIGITS_HEX = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };

  /** byte数组转hex字符数组 */
  protected static char[] encodeHex(byte[] data) {
    int l = data.length;
    char[] out = new char[l << 1];
    for (int i = 0, j = 0; i < l; i++) {
      out[j++] = DIGITS_HEX[(0xF0 & data[i]) >>> 4];
      out[j++] = DIGITS_HEX[0x0F & data[i]];
    }
    return out;
  }

  /** Hex字符数组转byte数组 */
  protected static byte[] decodeHex(char[] data) {
    int len = data.length;
    if ((len & 0x01) != 0) {
      throw new RuntimeException("字符个数应该为偶数");
    }
    byte[] out = new byte[len >> 1];
    for (int i = 0, j = 0; j < len; i++) {
      int f = toDigit(data[j], j) << 4;
      j++;
      f |= toDigit(data[j], j);
      j++;
      out[i] = (byte) (f & 0xFF);
    }
    return out;
  }

  protected static int toDigit(char ch, int index) {
    int digit = Character.digit(ch, 16);
    if (digit == -1) {
      throw new RuntimeException("Illegal hexadecimal character " + ch + " at index " + index);
    }
    return digit;
  }

  /** byte数组转16进制字符串 */
  public static String toHexString(byte[] bs) {
    if (bs == null) return null;
    return new String(encodeHex(bs));
  }

  /** 16进制字符串转byte数组 */
  public static byte[] decode(String hex) {
    if (hex == null) return null;
    return decodeHex(hex.toCharArray());
  }

  public static byte[] newAscii(int len) {
    byte[] ascii = new byte[len];
    for (int i = 0; i < len; i++) {
      ascii[i] = 0x30;
    }
    return ascii;
  }

  public static boolean isEmpty(final CharSequence cs) {
    return cs == null || cs.length() == 0;
  }

  public static boolean isNotEmpty(final CharSequence cs) {
    return !isEmpty(cs);
  }

  /** 16进制字符转为整数 */
  public static int hexCharToInteger(char ch) {
    try {
      if (ch >= '0' && ch <= '9') {
        return (int) (ch - '0');
      } else if (ch >= 'a' && ch <= 'f') {
        return (int) (ch - 'a' + 10);
      } else if (ch >= 'A' && ch <= 'F') {
        return (int) (ch - 'A' + 10);
      } else {
        throw new Exception("请确保传入为16进制数");
      }
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

}
